"""
Functions for demand management project, specifically using epanettools
Author: Janice Zhuang
Python 2.7
"""
from epanettools.epanettools import  Node, Link

# Network property variables
d = Node.value_type['EN_DEMAND']
p = Node.value_type['EN_PRESSURE']
hl = Link.value_type['EN_HEADLOSS']
e = Link.value_type['EN_ENERGY']
b = Node.value_type['EN_BASEDEMAND']
leng = Link.value_type['EN_LENGTH']
flow = Link.value_type['EN_FLOW']
q = Node.value_type['EN_QUALITY']
diam = Link.value_type['EN_DIAMETER']
# depends on epanet properties above

def getLinkValues(link,prop,ts_index,l):
    allValues = l[link].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

def getNodeValues(node,prop,ts_index,n):
    allValues = n[node].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

