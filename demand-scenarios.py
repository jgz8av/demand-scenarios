"""
demand scenarios project
Author: Janice Zhuang
Python 2.7, epanettools 0.0.9
"""
# for data organization and calculation
import numpy as np
import pandas as pd

# for EPANET files
import tempfile, os
from epanettools.epanettools import EPANetSimulation, Node, Link, Network, \
                                    Nodes, Links, Patterns, Pattern, \
                                    Controls, Control
#import janepanet as jep #Janice's own functions

# for plotting
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from matplotlib.lines import Line2D
import seaborn as sns # heat matrices
import networkx as nx # network graphs
import janplots as jp #Janice's own functions
from PlotNetwork import coloredFeatures #Janice's own functions

#-----INSTRUCTIONS-----#
# 1) Select User Inputs in section below
# 2) If want certain plots, need to uncomment those lines
# ** Please note, the heat map/matrices use the Seaborn (sns) color scheme.
# ** Once the sns color scheme is set, all other plots will use this too.
# ** To avoid this, choose to plot other plots first.
# 3) If want to write results to excel, need to uncomment those lines

#---------------------#
#-----USER INPUTS-----#
#---------------------#
# metrics: water age, water loss, energy loss, peak flow, flow, pressure
metrics = ['waterloss', 'headloss', 'pressure', 'age', 'flow']
#metrics = ['flow', 'pressure']

# networks ordered by pipe size:
# [Net 1=ky3, Net 2=ky5, Net 3=ky6, Net 4=ky7, Net 5=ky1, Net 6=ky8, Net 7=ky4, unused=ky2]
netDict = {'ky3':'1', 'ky5':'2', 'ky6':'3', 'ky7':'4', 'ky1':'5', 'ky8':'6', 'ky4':'7', 'ky2':'0'}
#networks = ['ky3', 'ky5', 'ky6', 'ky7', 'ky1', 'ky8', 'ky4']
#networks = ['ky3_v1', 'ky6_v1', 'ky1_v1'] # for energy - removed dummy reservoirs and pumps to check = OK
#networks = ['ky5_v2', 'ky8_v2'] # removed controls to fix energy inbalance/convergence issue
netDict = {'ky3_v1':'1', 'ky5_v2':'2', 'ky6_v1':'3', 'ky7':'4', 'ky1_v1':'5', 'ky8_v2':'6', 'ky4':'7', 'ky2':'0'}
networks = ['ky3_v1', 'ky5_v2', 'ky6_v1', 'ky7', 'ky1_v1', 'ky8_v2', 'ky4']
networks = ['ky4'] # benchmark network

# scenario: 'Reduc'=S1/conservation/reduced demand; 'Flat'=S2/load-shifting/flattened demand;
new = 'Reduc'
#new = 'Flat'
print new, "Scenario"

# percentile, for removing 'outliers'
percent = 95

# where the files are
dir = 'networks' # folder where the .inp files are
save_dir = 'results/figures/v2/' # for saving plots

#----------------------------------#
#-----GENERAL SIMULATION SETUP-----#
#----------------------------------#
# time: 24 hour sim, 1 hour pattern time steps, 30 min reporting time step
hr = 1
nt = 24/hr
hstep = 3600/2  # 30 min hydraulic time step
qstep = 3600/12 # 5 min quality time step

# Network property variables
d = Node.value_type['EN_DEMAND']
p = Node.value_type['EN_PRESSURE']
hl = Link.value_type['EN_HEADLOSS']
en = Link.value_type['EN_ENERGY']
b = Node.value_type['EN_BASEDEMAND']
leng = Link.value_type['EN_LENGTH']
flow = Link.value_type['EN_FLOW']
q = Node.value_type['EN_QUALITY']
diam = Link.value_type['EN_DIAMETER']
H = Node.value_type['EN_HEAD']


#-------------------#
#-----FUNCTIONS-----#
#-------------------#
# depends on epanet property variables above
def getTimeSteps(tsteps):
    # get correct time steps
    tsteps_iter = range(0, len(tsteps))
    ts_index = []   # length should be divisible by total extended time pd (hrs)
    i = 0   # (hour 0 to hour 24) = 49 elements for 1-hour pat, 24-hr pd
    tempsum = 0
    for index in tsteps_iter:
        # include 1st & last index
        if i==tsteps_iter[0] or i==tsteps_iter[-1]:
            ts_index.append(i)
        # include 1800L index
        elif tsteps[i]==hstep:
            ts_index.append(i)
        # if partial step, loop through new list to find full steps
        elif tempsum + tsteps[i] == 1800:
            ts_index.append(i)
            tempsum = 0
        else:
            tempsum += tsteps[i]
        i += 1
    return ts_index

def getLinkValues(link,prop,ts_index,l):
    allValues = l[link].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

def getNodeValues(node,prop,ts_index,n):
    allValues = n[node].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

def getLinkValuesMid(link,prop,ts_index,l,percent):
    allValues = l[link].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    cut1, cut2, correctValues = jp.midPercentile(correctValues,percent,"")
    return correctValues

def getNodeValuesMid(node,prop,ts_index,n,percent):
    allValues = n[node].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    cut1, cut2, correctValues = jp.midPercentile(correctValues,percent,"")
    return correctValues

def nodeLossAll(n,l,ts_index):
    # for time agg and node agg plots
    energyNodes = [node for node in list(n) if n[node].node_type==0]
    allNodeLosses = []
    for node in energyNodes:
        # list for pipes that connect
        pipes_connect = [i.index for i in n[node].links \
                         if l[i.index].link_type==1]
        # list for pipes that connect to a "specialty node"
        pipes_special = [pipe for pipe in pipes_connect \
                         if (l[pipe].start.index not in energyNodes) \
                         or (l[pipe].end.index not in energyNodes)]
        pipeLosses_list = [] # length of pipes_connect
        for pipe in pipes_connect:
            pipeLosses = np.array(getLinkValues(pipe,hl,ts_index,l)) # length of ts_index
            if pipe in pipes_special:
                pipeLosses = pipeLosses
            else:
                pipeLosses = 0.5*pipeLosses
            pipeLosses_list.append(pipeLosses)
        pipeLosses_list = np.array(pipeLosses_list)
        nodeLosses_list = np.sum(pipeLosses_list,axis=0) # sum by row (time), length of ts_index
        allNodeLosses.append(nodeLosses_list)
    return allNodeLosses

def nodeLossResults(n, l, ts_index):
    energyNodes = [node for node in list(n) if n[node].node_type==0]
    nodeloss_list = []
    nodeUnitloss_list = []
    for node in energyNodes:
        # list for pipes that connect
        pipes_connect = [i.index for i in n[node].links \
                         if l[i.index].link_type==1]
        # list for pipes that connect to a "specialty node"
        pipes_special = [pipe for pipe in pipes_connect \
                         if (l[pipe].start.index not in energyNodes) \
                         or (l[pipe].end.index not in energyNodes)]
        # sum(NOT special loss)/2 + sum(SPECIAL loss)
        special_loss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) \
                        for link in pipes_special]
        special_unitloss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent))/ \
                            l[link].results[leng][0] \
                            for link in pipes_special]
        notSpecial_loss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) \
                           for link in pipes_connect \
                           if link not in pipes_special]
        notSpecial_unitloss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent))/ \
                               l[link].results[leng][0] \
                               for link in pipes_connect \
                               if link not in pipes_special]
        nodeloss = sum(notSpecial_loss)/2 + sum(special_loss)
        nodeUnitloss = sum(notSpecial_unitloss)/2 + sum(special_unitloss)
        nodeloss_list.append(nodeloss)
        nodeUnitloss_list.append(nodeUnitloss)
    return nodeloss_list, nodeUnitloss_list

def ageResults(junctions,n,l,ts_index):
    ageDist_list = []  # age distribution across times
    ageDistMax_list = []   # max age for each node
    age24hr_list = []   # age @ hour 24 for each node
    for node in junctions:
        ageDist_list.append(getNodeValues(node,q,ts_index,n))
        age24hr_list.append(n[node].results[q][-1])
        maxAge = max(getNodeValues(node,q,ts_index,n))
        ageDistMax_list.append(maxAge)
    # average water age @ hour 24
    ageAvg = sum(age24hr_list)/float(len(age24hr_list))
    # median water age @ hour 24
    ageMed = np.median(np.array(age24hr_list))
    return age24hr_list, ageAvg, ageMed

def leakResults2(a,B,junctions,n,l,ts_index):
    leakNode_list = []
    for nodei in junctions:
        # connecting pipes
        connections = sorted([c.index for c in n[nodei].links])
        leakLink_list = []
        for link in list(connections):
            # get length
            lengthLink = l[link].results[leng][0] #ft
            # find the other node (node j)
            if l[link].start.index == nodei:
                nodej = l[link].end.index
            else:
                nodej = l[link].start.index
            # get all pressures of node i and node j
            pressi = np.array(getNodeValues(nodei,p,ts_index,n))
            pressj = np.array(getNodeValues(nodej,p,ts_index,n))
            # get all pressures of this link by taking average
            pressLink = 0.5*(pressi + pressj) #psi
            # take mid X% of the values
            cut1, cut2, pressLinkMid = jp.midPercentile(pressLink,percent,"")
            pressLinkMid = np.array(pressLinkMid)
            # calculate total leak @ this link = B*length*linkPress**a
            # 1ft = 0.3048m, 1.42197psi = 1m of head, 15.8503gpm = 1L/s
            leakLink = B*(lengthLink*0.3048)*((pressLinkMid/1.42197)**a)*15.8503
            # sum vector and add to list for all connecting links
            leakLink_list.append(sum(leakLink))
        # calculate total leak @ this node = 0.5*sum(all leakLinks)
        leakNode = 0.5*sum(leakLink_list)
        # add to list for all nodes
        leakNode_list.append(leakNode)
    leakNetwork = sum(leakNode_list)
    return leakNode_list, leakNetwork


def pressResults(n,ts_index):
    # junction list without I-Pump, O-Pump, I-RV, O-RV
    junctions = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]
    avgpress_list = []
    peakpress_list = []
    medpress_list = []
    allpressres_list = []
    for node in junctions:
        press_results = getNodeValues(node,p,ts_index,n)
        allpressres_list.append(press_results)
        # average
        avgpress = sum(press_results)/len(press_results)
        avgpress_list.append(avgpress)
        # peak
        peakpress = max(press_results)
        peakpress_list.append(peakpress)
        # median
        medpress = np.median(np.array(press_results))
        medpress_list.append(medpress)
    avgMedpress = np.mean(medpress_list)
    avgPeakPress = sum(peakpress_list)/len(peakpress_list)
    return avgpress_list, peakpress_list, medpress_list, \
            avgMedpress, avgPeakPress, allpressres_list

def flowResults(l,ts_index):
    avgflow_list = []
    peakflow_list = []
    for link in list(l):
        # for pipes
        if l[link].link_type==1:
            flow_results = getLinkValuesMid(link,flow,ts_index,l,percent)
            abs_flow_results = []
            for result in flow_results:
                abs_result = abs(result)
                abs_flow_results.append(abs_result)
            # average flows
            avgflow = sum(abs_flow_results)/len(abs_flow_results)
            avgflow_list.append(avgflow)
            # peak flows
            peakflow = max(abs_flow_results)
            peakflow_list.append(peakflow)
    return avgflow_list, peakflow_list

# Energy-Breakdown functions

def HdResults(nodeType_List, ts_index, n):
    # energy results for nodes
    master_Hd_list = [] # head*demand by time, for all nodes
    for node in nodeType_List:
        # get heads for all  [ft/30min]
        heads = getNodeValues(node,H,ts_index,n)
        # get demands for all times [gal/30min]
        demands = getNodeValues(node,d,ts_index,n)
        # get head * demand for all times [gal/30min * ft/30min]
        headDems = np.multiply(heads,demands)
        master_Hd_list.append(headDems)
    master_df = pd.DataFrame(master_Hd_list) # make dataframe
    # convert units [gal/30min * ft]
    master_df = master_df.multiply(30)
    # node as user (positive H*d)
    pos_master_df = master_df[master_df >=0].fillna(0)
    pos_master_df.loc['Total'] = pos_master_df.sum(axis=0) # sum over all nodes for each time
    # node as source (negative H*d)
    neg_master_df = master_df[master_df <=0].fillna(0)
    neg_master_df.loc['Total'] = neg_master_df.sum(axis=0) # sum over all nodes for each time
    # return list Hd for all times (summed by node) [gal/30min * ft]
    user_Hd_list = list(pos_master_df.loc[['Total']].values.flatten())
    source_Hd_list = list(neg_master_df.loc[['Total']].values.flatten())
    return user_Hd_list, source_Hd_list

def leak_HdResults(a,B,junctions,n,l,ts_index):
    # Return dataframes of len = len(correct timesteps) = 49
    # So need to get leak results for ALL junctions by TIME
    # So NOT taking mid 95% of values anymore.
    # So everything the same up to leakLink (except remove pressLinkMid)
    # get demand of leaks
    leakNode_list = []
    for nodei in junctions:
        # connecting pipes
        connections = sorted([c.index for c in n[nodei].links])
        for link in list(connections):
            # get length
            lengthLink = l[link].results[leng][0] #ft
            # find the other node (node j)
            if l[link].start.index == nodei:
                nodej = l[link].end.index
            else:
                nodej = l[link].start.index
            # get all pressures of node i and node j
            pressi = np.array(getNodeValues(nodei,p,ts_index,n))
            pressj = np.array(getNodeValues(nodej,p,ts_index,n))
            # get all pressures of this link by taking average
            pressLink = 0.5*(pressi + pressj) #psi
            # calculate total leak @ this link = B*length*linkPress**a
            # 1ft = 0.3048m, 1.42197psi = 1m of head, 15.8503gpm = 1L/s
            leakLink = B*(lengthLink*0.3048)*((pressLink/1.42197)**a)*15.8503
        # Only 1/2 of the leakage values gets assigned to nodei
        leakNode = np.multiply(leakLink,0.5)
        # add to list for all nodes
        leakNode_list.append(leakNode)
    # make it into a dataframe w/ 49 rows [gal/30min]
    leakNode_df = pd.DataFrame(leakNode_list)
    # get heads of junctions (for leaks)
    master_heads_list = []
    for node in junctions:
        # get heads for all times [ft/30min]
        heads = getNodeValues(node,H,ts_index,n)
        master_heads_list.append(heads)
    master_heads_df = pd.DataFrame(master_heads_list)
    master_df = master_heads_df.multiply(leakNode_df) # [gal/30min * ft/30min]
    # convert units [gal/30min * ft]
    master_df = master_df.multiply(30)
    # node as user (positive H*d)
    pos_master_df = master_df[master_df >=0].fillna(0)
    pos_master_df.loc['Total'] = pos_master_df.sum(axis=0) # sum over all nodes for each time
    # node as source (negative H*d)
    neg_master_df = master_df[master_df <=0].fillna(0)
    neg_master_df.loc['Total'] = neg_master_df.sum(axis=0) # sum over all nodes for each time
    # return list Hd for all times (summed by node) [gal/30min * ft]
    user_Hd_list = list(pos_master_df.loc[['Total']].values.flatten())
    source_Hd_list = list(neg_master_df.loc[['Total']].values.flatten())
    return user_Hd_list, source_Hd_list

def hlQResults(linkType_List, ts_index, l):
    # energy results for links
    master_hlQ_list = [] # headloss*flow by time, for all links
    if linkType_List.size==0:
        loss_hlQ_list = list(0*np.ones(49))
        source_hlQ_list = list(0*np.ones(49))
    else:
        for link in linkType_List:
            # get heads (headloses) for all times [ft/30min]
            headlosses = getLinkValues(link,hl,ts_index,l)
            # get flows for all times [gal/30min]
            flows = getLinkValues(link,flow,ts_index,l)
            abs_flows = [abs(flo) for flo in flows]
            # get abs(head * flows) for all times [gal/30min * ft/30min]
            headlossFlows = np.multiply(headlosses,abs_flows)
            master_hlQ_list.append(headlossFlows)
        master_df = pd.DataFrame(master_hlQ_list) # make dataframe  [gal/30min * ft/30min]
        # convert units [gal/30min * ft]
        master_df = master_df.multiply(30)
        # link as user (positive hl*Q)
        pos_master_df = master_df[master_df >=0].fillna(0)
        pos_master_df.loc['Total'] = pos_master_df.sum(axis=0) # sum over all links for each time
        # link as source (negative hl*Q)
        neg_master_df = master_df[master_df <=0].fillna(0)
        neg_master_df.loc['Total'] = neg_master_df.sum(axis=0) # sum over all links for each time
        # return list hlQ for all times (summed by link) [gal/30min * ft]
        loss_hlQ_list = list(pos_master_df.loc[['Total']].values.flatten())
        source_hlQ_list = list(neg_master_df.loc[['Total']].values.flatten())
    return loss_hlQ_list, source_hlQ_list

def pumpEnergyResults(pumps_list, ts_index, l):
    # energy results for pumps (kwatts) - with default efficiency @ 75%
    master_kW_list = []
    for pump in pumps_list:
        energies = getLinkValues(pump,en,ts_index,l)
        master_kW_list.append(energies)
    master_df = pd.DataFrame(master_kW_list)
    master_df.loc['Total'] = master_df.sum(axis=0)
    kW_list = list(master_df.loc[['Total']].values.flatten())
    return kW_list

def DemandResults(nodeType_List, ts_index, n):
    # energy results for nodes
    master_Hd_list = [] # head*demand by time, for all nodes
    for node in nodeType_List:
        # get demands for all times [gal/30min]
        demands = getNodeValues(node,d,ts_index,n)
        master_Hd_list.append(demands)
    master_df = pd.DataFrame(master_Hd_list) # make dataframe
    # node as user (positive H*d)
    pos_master_df = master_df[master_df >=0].fillna(0)
    pos_master_df.loc['Total'] = pos_master_df.sum(axis=0) # sum over all nodes for each time
    # node as source (negative H*d)
    neg_master_df = master_df[master_df <=0].fillna(0)
    neg_master_df.loc['Total'] = neg_master_df.sum(axis=0) # sum over all nodes for each time
    # return list Hd for all times (summed by node) [gal/30min]
    user_Hd_list = list(pos_master_df.loc[['Total']].values.flatten())
    source_Hd_list = list(neg_master_df.loc[['Total']].values.flatten())
    return user_Hd_list, source_Hd_list

#def pumpEnergyResults(pumps_list,l,ts_index):
#    kwenergy_list = []
#    for pump in pumps_list:
#        # get energy in kwatts
#        kw_results = getLinkValues(link,e,ts_index,l)
#        flow_results = getLinkValues(link,flow,ts_index,l)
#        # get flow in MG
#        abs_flow_results = []
#        for result in flow_results:
#            abs_result = abs(result)
#            abs_flow_results.append(abs_result)
#        # gpm --> MGhr
#        MGhr_flow = sum(abs_flow_results)*60/(10**6)
#        kwenergy_list.append(sum(kw_results)/MGhr_flow)   #kw-hr/MG
#    return kwenergy_list
#
#def costFlowResults(weightParam,pipes_list,l,ts_index):
#    # returns the total cost of flow = sum over all edges of
#    # the product of the edge's flow and weight
#    weight = weightParam # typically head loss, for our purposes
#    eCostsList = []
#    for pipe in pipes_list:
#        # for 1 pipe @ all times
#        eFlow = np.array(np.fabs(getLinkValues(pipe,flow,ts_index,l)))
#        eHL = np.array(getLinkValues(pipe,weight,ts_index,l))
#        eCost = np.multiply(eFlow,eHL)
#        # add sum to list for all pipes
#        eCostsList.append(sum(eCost))
#    costOfFlow = sum(eCostsList)
#    return costOfFlow

#-----------------------------#
#-----FOR STORING RESULTS-----#
#-----------------------------#

# network properties
nodeNoList=[]
pipeNoList=[]
pumpNoList=[]
resNoList=[]
tankNoList=[]
valveNoList=[]
lengthList=[]
totbdList=[]

# percent differences
pctDiffwLoss = []
pctDiffwAge = []
pctDiffeLoss = []
pctDiffpFlow = []

# metric results, nominal pattern
allLoss=[]
allnLeak=[]
allAge=[]
appList = []
apfList=[]

# metric results, new pattern
allLossF=[]
allnLeakF=[]
allAgeF=[]
appListF=[]
apfListF=[]

#-----------------------------#
#-----INITIATE SIMULATION-----#
#-----------------------------#

# note, for ease, properties such as time step, accuracy, etc are preset in .inp file

for net in networks:

    print "Network: ", net
    # File for network (.inp)
    file = net + '.inp'
    path = os.path.join(os.getcwd(), dir, file)

    es = EPANetSimulation(path) # EPANetSimulation opens & closes file

    #---------------------------------------#
    #-----DEFINE PATTERNS FOR SCENARIOS-----#
    #---------------------------------------#

    # Define NOMINAL demand pattern
    # S0/Base
    p1_index = 1
    p1 = es.network.patterns[p1_index]
    p1_list = list(p1.values())[0:24]
    pat1 = p1_list
    avg_pat = round(sum(p1_list)/len(p1_list),4)

    # Define FLATTENED demand pattern
    # S2/Load-shifting
    p2_index = 2
    p2 = es.network.patterns[p2_index]
    p2_list = []
    [p2_list.append(avg_pat) for i in range(1,nt+1)]

    # Define REDUCED demand pattern
    # S1/Conservation
    p3_index = 3
    p3 = es.network.patterns[p3_index]
    p3_list = list(p3.values())[0:24]

    #-----------------------------#
    #-----SET NOMINAL PATTERN-----#
    #-----------------------------#

    # Set pattern
    ret = es.ENsetpattern(p1_index, p1_list)
    if ret != 0:
        print ret

    # Permanently change values = write to new file
    f1 = os.path.join(tempfile.gettempdir(), "temp.inp")
    ret = es.ENsaveinpfile(f1)
    if ret != 0:
        print ret

    # Open, run sim, close file with EPANetSim
    es1 = EPANetSimulation(f1)
    n = es1.network.nodes
    l = es1.network.links

    # Run new simulations
    error = es1.run()
    errorq = es1.runq()
    # get errors?
    print "Errors: ", error, errorq

    # Get correct time steps
    tsteps = es1.network.tsteps
    ts_index = getTimeSteps(tsteps)

    #---------------------------------------------#
    #-----GET GENERAL NETWORK CHARACTERISTICS-----#
    #---------------------------------------------#

    pipes_list = np.array([pipe for pipe in list(l) if l[pipe].link_type==1])
    node_list = np.array(list(n))
    pumps_list = np.array([link for link in list(l) if l[link].link_type==2])
    valves_list = np.array([link for link in list(l) if l[link].link_type in range(3,9)])
    res_list = np.array([node for node in list(n) if n[node].node_type==1])
    tanks_list = np.array([node for node in list(n) if n[node].node_type==2])

    # nodes
    nodeNo = len(node_list)
    nodeNoList.append(nodeNo)

    # pipes
    pipeNo = len(pipes_list)
    pipeNoList.append(pipeNo)

    # pumps
    pumpNo = len(pumps_list)
    pumpNoList.append(pumpNo)

    # reservoirs
    resNo = len(res_list)
    resNoList.append(resNo)

    # tanks
    tankNo = len(tanks_list)
    tankNoList.append(tankNo)

    # valves
    valveNo = len(valves_list)
    valveNoList.append(valveNo)

    # length of pipes (Kft)
    length = sum([l[link].results[leng][0] for link in list(l)]) / 1000
    lengthList.append(round(length,2))

    # total base demand (MGD)
    basedem = [n[node].results[b][0] for node in list(n) \
               if n[node].node_type==0]
    totbasedem = sum(basedem) * 60 * 24 / (10**6)
    totbdList.append(round(totbasedem,2))

    #---------------------#
    #-----GET RESULTS-----#
    #---------------------#

    # check for nodes recording negative or low pressure (PSI)
    pNeg = 0
    pLow = 40   # considered low pressure
    pHi = 100   # max psi
    neg_press = sorted([y.id for x,y in n.items() \
                        if round(min(y.results[p]),1)<pNeg])
    low_press = sorted([y.id for x,y in n.items() \
                        if round(min(y.results[p]),1)<pLow \
                        and y.id not in neg_press])
    hi_press = sorted([y.id for x,y in n.items() \
                       if round(min(y.results[p]),1)>pHi])

    # junction list, without I-Pump, O-Pump, I-RV, O-RV
    junctions = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]

    # BACKGROUND LEAKAGE (gpm or gpd), by junction
    # leakage parameters:
    a = 1.5 # for background losses through area than changes linearly w/ press (May 1994)
    B = 10**(-7)    # a reasonable guess for our purposes (refer to sources in Zhuang thesis)
    # nodal leakage - computed from leakage along pipe model
    # [leaks by junction, total background leakage in network]
    tot_bgLeaks, leakSum = leakResults2(a,B,junctions,n,l,ts_index)
    wLossResult = round(leakSum*30,2) #(gpd), total background leakage in system
    allnLeak.append(wLossResult) # add to master list

    # WATER AGE @ END OF SIM (hr), by junction
    # [age at 24 hours by junction, average age at 24 hours, median age at 24 hours]
    age24hr_list, ageAvg, ageMed = ageResults(junctions,n,l,ts_index)
    ageResult = round(ageMed,2) #(hr), median age @ 24 hr in system
    allAge.append(ageResult) # add to master list

    # HEAD LOSSES (ft)
    # by pipes
    tot_losses = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) for link in list(l) \
                  if l[link].link_type==1] #(ft)
    # by nodes except sources
    # where energy loss at node = sum(loss of all connecting pipes)/2
    # [head losses by node, unit head losses by node]
    nodeloss_list, nodeUnitloss_list = nodeLossResults(n,l,ts_index)
    nLossResult = round(sum(nodeloss_list)*30,2) #(ft), total head loss in system
    allLoss.append(nLossResult) # add to master list

    # FLOWS (gpm), by pipe
    # [avg flows by pipe, peak flows by pipe]
    avgflow_list, peakflow_list = flowResults(l,ts_index)
    pkFlowResult = round(np.median(peakflow_list),2) #(gpm), median peak flow in system
    apfList.append(pkFlowResult) # add to master list

    # PRESSURES (psi), by junction
    # [avg press by junc, pk press by junc, med press by junc, \
    # avg med. press, avg pk press, all press by junc]
    avgpress_list, peakpress_list, medpress_list, \
    avgMedpress, avgPeakPress, allPressResults = pressResults(n,ts_index)
    pkPressResult = avgPeakPress #(psi), average peak pressure in system
    appList.append(avgPeakPress) # add to master list

    # ENERGY 2.0
    #--- Inputs: Reservoirs, Pumps, Tanks-out
    # get Hd for reservoirs
    resUser_Hd, resSource_Hd = HdResults(res_list,ts_index,n) # [gal/30min * ft]
    resSource_Hd_sum = sum(resSource_Hd)*30 # [gpd * ft]
    # get hlQ for pumps
    pumpUser_hlQ, pumpSource_hlQ = hlQResults(pumps_list,ts_index,l) # [gal/30min * ft]
    pump_hlQ_sum = sum(pumpSource_hlQ)*30 # [gpd * ft]
    # get Hd for tanks-out
    tankUser_Hd, tankSource_Hd = HdResults(tanks_list,ts_index,n) # [gal/30min * ft]
    tankSource_Hd_sum = sum(tankSource_Hd)*30 # [gpd * ft]
    # sum for Pin
#    sum_Pin = res_Hd_sum + pump_hlQ_sum + tankOut_Hd_sum
    sum_Pin = resSource_Hd_sum + (pump_hlQ_sum) + tankSource_Hd_sum # [gpd * ft]

    #--- Outputs: Consumers, Leaks, Tanks-in
    # get Hd for junctions
    juncUser_Hd, juncSource_Hd = HdResults(junctions,ts_index,n) # [gal/30min * ft]
    junc_Hd_sum = sum(juncUser_Hd)*30 # [gpd * ft]
    # get Hd for tanks-in
    tankUser_Hd_sum = sum(tankUser_Hd) *30 # [gpd * ft]
    resUser_Hd_sum = sum(resUser_Hd)*30 # [gpd * ft]
    # get Hd for leaks
    leakUser_Hd, leakSource_Hd = leak_HdResults(a,B,junctions,n,l,ts_index) # [gal/30min * ft]
    leak_Hd_sum = sum(leakUser_Hd)*30 # [gpd * ft]
    # sum Pout
#    sum_Pout = junc_Hd_sum + tankIn_Hd_sum
    sum_Pout = junc_Hd_sum + tankUser_Hd_sum + resUser_Hd_sum # [gpd * ft]
    sum_Pout_leaks = junc_Hd_sum + tankUser_Hd_sum + resUser_Hd_sum + leak_Hd_sum # [gpd * ft]

    #--- Losses: Friction, Valves
    # get hlQ for pipes
    pipeUser_hlQ, pipeSource_hlQ = hlQResults(pipes_list,ts_index,l) # [gal/30min * ft]
    pipe_hlQ_sum = sum(pipeUser_hlQ)*30 # [gpd * ft]
    # get hlQ for valves
    valveUser_hlQ, valveSource_hlQ = hlQResults(valves_list,ts_index,l) # [gal/30min * ft]
    valve_hlQ_sum = sum(valveUser_hlQ)*30 # [gpd * ft]
    # sum Plosses
    sum_Ploss = pipe_hlQ_sum + valve_hlQ_sum # [gpd * ft]

    #--- Check Balanced? sum of Pin (-) and Pout-all (+) = Perror
    Perror = (sum_Pin + (sum_Pout + sum_Ploss)) # [gpd * ft]
    Perror_leaks = Perror = (sum_Pin + (sum_Pout_leaks + sum_Ploss)) # [gpd * ft]

#    # PUMP ENERGY (kw-hr/MG)
#    pumpEnergy_list = pumpEnergyResults(pumps_list,l,ts_index)
#    ePumpResult = round(sum(pumpEnergy_list),2)
#    pumpEnergy.append(ePumpResult)
#
#    # ENERGY INTENSITY (ft/MGD)
#    eIntensResult = round(sum(nodeloss_list)*30/totbasedem,2)
#    energyIntens.append(eIntensResult)
#
#    # COST OF FLOW - HEAD LOSS (ft*gpm)
#    totCost = costFlowResults(hl,pipes_list,l,ts_index)
#    totCostResult = round(totCost,2)
#    HLCostList.append(totCostResult)

    #-----------------------#
    #-----PRINT RESULTS-----#
    #-----------------------#

#    print "For the NOMINAL DEMAND PATTERN:"
#    print ""
#    print "Pressure < 0: ", neg_press
#    print "Pressure < 40: ", len(low_press)
#    print "Pressure > 100: ", len(hi_press)
#    print avgMedpress, "psi"
#
#    print "Total background leakage is ", round(wLossResult/10**6,3), " MGD"
#    print "The median water age for all nodes @ 24 hrs is ", ageResult, " hr"
##    print "The total head loss in pipes is " , round(sum(tot_losses)*30,2), " ft"
#    print "The total head loss by nodes is " , round(nLossResult/1000,3), " k-ft"
#    print "Median peak flow is", pkFlowResult, "gpm"

    #------------------------------------#
    #-----SET 'NEW SCENARIO' PATTERN-----#
    #------------------------------------#

    if new == 'Flat':
        # Set FLAT pattern (changing pattern 1 to values of flattened)
        ret = es1.ENsetpattern(p1_index, p2_list)
        if ret != 0:
            print ret
        avgpat = round(np.mean(np.array(p2_list)),1)
        scen = '2'
    else:
        # Set REDUCED pattern (changing pattern 1 to values of reduced)
        ret = es1.ENsetpattern(p1_index, p3_list)
        if ret != 0:
            print ret
        avgpat = round(np.mean(np.array(p3_list)),1)
        scen = '1'

    # Permanently change values = write to new file
    fNew = os.path.join(tempfile.gettempdir(), "tempf.inp")
    ret = es1.ENsaveinpfile(fNew)
    if ret != 0:
        print ret

    # Open, run sim, close file with EPANetSim
    esf = EPANetSimulation(fNew)
    nF = esf.network.nodes
    lF = esf.network.links

    # Run new sims
    error = esf.run()
    errorq = esf.runq()
    # get errors?
    print "Errors: ", error, errorq

    # Get correct time steps
    tstepsF = esf.network.tsteps
    ts_indexF = getTimeSteps(tstepsF)

    #---------------------#
    #-----GET RESULTS-----#
    #---------------------#

    # check for nodes recording negative & low pressure (psi)
    neg_pressF = sorted([y.id for x,y in nF.items() \
                        if round(min(y.results[p]),1)<pNeg])
    low_pressF = sorted([y.id for x,y in nF.items() \
                        if round(min(y.results[p]),1)<pLow \
                        and y.id not in neg_press])
    hi_pressF = sorted([y.id for x,y in nF.items() \
                       if round(min(y.results[p]),1)>pHi])

    # BACKGROUND LEAKAGE (gpm or gpd), by junction
    # leakage parameters: same as NOMINAL PATTERN
    # [leaks by junction, total background leakage in network]
    tot_bgLeaksF, leakSumF = leakResults2(a,B,junctions,nF,lF,ts_indexF)
    wLossResultF = round(leakSumF*30,2) #(gpd), total background leakage in system
    allnLeakF.append(wLossResultF)

    # WATER AGE @ END OF SIM (hr), by junction
    # [age at 24 hours by junction, average age at 24 hours, median age at 24 hours]
    age24hrF_list, ageAvgF, ageMedF = ageResults(junctions,nF,lF,ts_indexF)
    ageResultF = round(ageMedF,2) #(hr), median age @ 24 hr in system
    allAgeF.append(ageResultF)

    # HEAD LOSSES (ft)
    # by pipes
    tot_lossesF = [sum(getLinkValuesMid(link,hl,ts_indexF,lF,percent)) for link in list(lF) \
                   if lF[link].link_type==1] #(ft))
    # by nodes except sources
    # [head losses by node, unit head losses by node]
    nodelossF_list, nodeUnitlossF_list = nodeLossResults(nF,lF,ts_indexF)
    nLossResultF = round(sum(nodelossF_list)*30,2) #(ft), total head loss in system
    allLossF.append(nLossResultF) #ft

    # FLOWS (gpm), by pipe
    # [avg flows by pipe, peak flows by pipe]
    avgflowF_list, peakflowF_list = flowResults(lF,ts_indexF)
    pkFlowResultF = round(np.median(peakflowF_list),2) #(gpm), median peak flow in system
    apfListF.append(pkFlowResultF) #gpm

    # PRESSURES (psi), by junction
    # [avg press by junc, pk press by junc, med press by junc, \
    # avg med. press, avg pk press, all press by junc]
    avgpressF_list, peakpressF_list, medpressF_list, \
    avgMedpressF, avgPeakPressF, allPressResultsF = pressResults(nF,ts_indexF)
    pkPressResultF = avgPeakPressF #(psi), average peak pressure in system
    appListF.append(pkPressResultF)

    # ENERGY 2.0
    #--- Inputs: Reservoirs, Pumps, Tanks-out
    # get Hd for reservoirs
    resUser_HdF, resSource_HdF = HdResults(res_list,ts_indexF,nF) #[gal/30min * ft]
    resSource_HdF_sum = sum(resSource_HdF)*30 #[gpd * ft]
    # get hlQ for pumps
    pumpUser_hlQF, pumpSource_hlQF = hlQResults(pumps_list,ts_indexF,lF) #[gal/30min * ft]
    pump_hlQF_sum = sum(pumpSource_hlQF)*30 #[gpd * ft]
    # get Hd for tanks-out
    tankUser_HdF, tankSource_HdF = HdResults(tanks_list,ts_indexF,nF) #[gal/30min * ft]
    tankSource_HdF_sum = sum(tankSource_HdF)*30 #[gpd * ft]
    # sum for Pin
#    sum_PinF = res_HdF_sum + pump_hlQF_sum + tankOut_HdF_sum
    sum_PinF = resSource_HdF_sum + pump_hlQF_sum + tankSource_HdF_sum #[gpd * ft]

    #--- Outputs: Consumers, Leaks, Tanks-in
    # get Hd for junctions
    juncUser_HdF, juncSource_HdF = HdResults(junctions,ts_indexF,nF) #[gal/30min * ft]
    junc_HdF_sum = sum(juncUser_HdF)*30 #[gpd * ft]
    # get Hd for tanks-in
    tankUser_HdF_sum = sum(tankUser_HdF)*30 #[gpd * ft]
    resUser_HdF_sum = sum(resUser_HdF)*30 #[gpd * ft]
    # get Hd for leaks
    leakUser_HdF, leakSource_HdF = leak_HdResults(a,B,junctions,nF,lF,ts_indexF) #[gal/30min * ft]
    leak_HdF_sum = sum(leakUser_HdF)*30 #[gpd * ft]
    # sum Pout
#    sum_PoutF = junc_HdF_sum + tankIn_HdF_sum
    sum_PoutF = junc_HdF_sum + tankUser_HdF_sum + resUser_HdF_sum #[gpd * ft]
    sum_PoutF_leaks = junc_HdF_sum + tankUser_HdF_sum + resUser_HdF_sum + leak_HdF_sum #[gpd * ft]
    #--- Losses: Friction, Valves
    # get hlQ for pipes
    pipeUser_hlQF, pipeSource_hlQF = hlQResults(pipes_list,ts_indexF,lF) #[gal/30min * ft]
    pipe_hlQF_sum = sum(pipeUser_hlQF)*30 #[gpd * ft]
    # get hlQ for valves
    valveUser_hlQF, valveSource_hlQF = hlQResults(valves_list,ts_indexF,lF) #[gal/30min * ft]
    valve_hlQF_sum = sum(valveUser_hlQF)*30 #[gpd * ft]
    # sum Plosses
    sum_PlossF = pipe_hlQF_sum + valve_hlQF_sum #[gpd * ft]

    #--- Check Balanced? Difference between Pin and Pout-all = Perror
    PerrorF = (sum_PinF + (sum_PoutF + sum_PlossF)) #[gpd * ft]
    PerrorF_leaks = (sum_PinF + (sum_PoutF_leaks + sum_PlossF)) #[gpd * ft]

    print Perror
    print PerrorF
    print ""
    print Perror_leaks
    print PerrorF_leaks

#    # PUMP ENERGY (kw-hr/MG)
#    pumpEnergyF_list = pumpEnergyResults(pumps_list,lF,ts_indexF)
#    pumpEnergyF.append(round(sum(pumpEnergyF_list),2))
#
#    # ENERGY INTENSITY (ft/MGD)
#    energyIntensF.append(round(sum(nodelossF_list)*30/(totbasedem*avgpat),2))
#
#    # COST OF FLOW - HEAD LOSS (ft*gpm)
#    totCostF = costFlowResults(hl,pipes_list,lF,ts_indexF)
#    HLCostListF.append(round(totCostF,2))

    #-----------------------#
    #-----PRINT RESULTS-----#
    #-----------------------#

#    print ""
#    if new == 'Flat':
#        print "For the FLATTENED DEMAND PATTERN:"
#    else:
#        print "For the REDUCED DEMAND PATTERN:"
#    print ""
#
#    print "Pressure < 0: ", neg_pressF
#    print "Pressure < 40: ", len(low_pressF)
#    print "Pressure > 100: ", len(hi_pressF)
#    print avgMedpressF, "psi"
#    print ""
#
#    print "Total background leakage is ", round(wLossResultF/10**6,3), " MGD"
#    print "Median water age for all nodes @ 24 hrs is ", ageResultF, " hr"
##    print "The total head loss in pipes is " , round(sum(tot_lossesF)*30,2), " ft"
#    print "The total head loss in junctions is " , round(nLossResultF/1000,3), " k-ft"
#    print "Median peak flow is", pkFlowResultF, "gpm"
#    print ""

    #-----------------------------------------------------#
    #-----DELTAS and PERCENT DIFFERENCES FROM NOMINAL-----#
    #-----------------------------------------------------#

    # Delta Age @ 24 hr by Node (New - Base)
    delta_age24hr, pct_age24hr = jp.pctDiff(age24hr_list, age24hrF_list)

    # Background Leakage by Node (New - Base)
    delta_nLeak, pct_nLeak = jp.pctDiff(tot_bgLeaks, tot_bgLeaksF)

    # Energy Loss by Pipe (New - Base)
    delta_loss, pct_loss = jp.pctDiff(tot_losses, tot_lossesF)
    # Energy Loss by Node (New - Base)
    delta_nloss, pct_nloss = jp.pctDiff(nodeloss_list, nodelossF_list)

    # Flow, average
    delta_avgflow, pct_avgflow = jp.pctDiff(avgflow_list, avgflowF_list)
    # Flow, peak
    delta_peakflow, pct_peakflow = jp.pctDiff(peakflow_list, peakflowF_list)

    # Pressure, average
    delta_avgpress, pct_avgpress = jp.pctDiff(avgpress_list, avgpressF_list)
    # Pressure, peak
    delta_peakpress, pct_peakpress = jp.pctDiff(peakpress_list, peakpressF_list)
    # Pressure, median
    delta_allPressRes, pct_allPressRes = jp.pctDiff(allPressResults, allPressResultsF)

    # Percent differences of deltas
    pctDiffwLoss.append(100*(wLossResultF - wLossResult)/wLossResult)
    pctDiffwAge.append(np.median(pct_age24hr))
    pctDiffeLoss.append(100*(nLossResultF - nLossResult)/nLossResult)
    pctDiffpFlow.append(np.median(pct_peakflow))


#---------------------------#
#-----PLOTTING PATTERNS-----#
#---------------------------#

    # time
    time_L = esf.network.time
    times = [t/3600 for t in time_L if (t % 3600 == 0)]
    times = range(1,25)
    timesTicks = np.arange(4,25,4)
    yTicks = np.arange(0,2.5,0.5)

    # rounding
    reduced = 0.50  # reduced demand percentage (Gurung et al. 2015)
    p3_list = np.round(np.array(p1_list) * reduced,2)

    # delta differences in demand profiles
    p00_list = np.array(p1_list) - np.array(p1_list)
    p10_list = np.array(p3_list) - np.array(p1_list)
    p20_list = np.array(p2_list) - np.array(p1_list)

    #------------------------#
    #-----PLOT: PATTERNS-----#
    #------------------------#

#    fsize=14 # 14pt for 0.75 latex scale to be like 12pt
#    fig = plt.figure(figsize=(6,3))
#    ax = fig.add_subplot(111)
#    # colors, for thesis
#    plt.plot(times, p1_list, '-', color='grey', markerfacecolor='none', markersize=8, linewidth=3)
#    plt.plot(times, p3_list, '--', color='C0', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p2_list, '-', color='yellowgreen', markerfacecolor='none', markersize=8, linewidth=3)
##    # black and white, for journal
##    plt.plot(times, p1_list, '-', color='grey', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p3_list, 'k--', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p2_list, 'k-', markerfacecolor='none', markersize=8, linewidth=3)
#
#    plt.legend(['S0',
#                'S1'],frameon=False,fontsize=fsize,loc=9,bbox_to_anchor=(0.5,-0.20),ncol=3)
#    plt.xlabel('Time',fontsize=fsize)
#    plt.ylabel('Demand Multiplier',fontsize=fsize)
#    plt.xlim(1,max(times))
#    plt.xticks(range(4,25,4), timesTicks,fontsize=fsize)
#    plt.ylim(0,max(p1_list)+0.25)
#    plt.yticks(yTicks,yTicks,fontsize=fsize)
#    ax.yaxis.set_major_formatter(FormatStrFormatter('%0.1f'))
#    fig.tight_layout()
#    fig.subplots_adjust(bottom=0.26)
#    fig.savefig(save_dir + 'pattern.pdf', format='pdf', dpi=100)

    #------------------------------#
    #-----PLOT: DELTA PATTERNS-----#
    #------------------------------#

#    fig = plt.figure(figsize=(6,3))
#    ax = fig.add_subplot(111)
#   # colors
#    plt.plot(times, p10_list, 'C0--', markerfacecolor='none', markersize=8, linewidth=3)
#    plt.plot(times, p20_list, '-', color='yellowgreen', markerfacecolor='none', markersize=8, linewidth=3)
##    # black and white, for journal
##    plt.plot(times, p10_list, 'k--', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p20_list, 'k-', markerfacecolor='none', markersize=8, linewidth=3)
#    plt.legend(['S1-S0',
#                'S2-S0'],frameon=False,fontsize=fsize-1,loc=9,bbox_to_anchor=(0.5,-0.20),ncol=2)
#    plt.axhline(0, color='grey', linewidth=3, alpha=0.75)
#    plt.xlabel('Time',fontsize=fsize)
#    plt.ylabel(r'$\Delta$ Multiplier',fontsize=fsize)
#    plt.xlim(1,max(times))
#    plt.xticks(range(4,25,4), timesTicks,fontsize=fsize)
#    plt.ylim(-1.,1.)
#    plt.yticks(fontsize=fsize)
#    ax.yaxis.set_major_formatter(FormatStrFormatter('%0.1f'))
#    fig.tight_layout()
#    fig.subplots_adjust(bottom=0.26)
#    fig.savefig(save_dir+'patternDifference.png', format='png', dpi=100)

#----------------------------#
#-----ENERGY RESULTS 2.0-----#
#----------------------------#
#    # WORK IN PROGRESS
#    # Plots
#    fig = plt.figure(figsize=(12,4))
#    ax = fig.add_subplot(111)
#    ax.plot(np.array(resUser_Hd) - np.array(resSource_Hd), label='Res')
#    ax.plot(np.multiply(-1,pumpSource_hlQ), label='Pump')
#    ax.plot(np.array(tankUser_Hd) - np.array(tankSource_Hd), label='Tank')
#    ax.plot(np.multiply(-1,juncUser_Hd), label='Consumer')
#    ax.plot(np.multiply(-1,np.add(pipeUser_hlQ, valveUser_hlQ)), label='Losses')
#    ax.set_xlabel('Time')
#    ax.set_ylabel('Energy (gpm*ft)')
#    ax.set_xlim([0,48])
#    ax.legend(loc=9,bbox_to_anchor=(0.5,-0.20),ncol=5)
#    fig.tight_layout()
#    fig.savefig('S0EnergyBreakdown_'+net+'.png', format='png', dpi=300)
#    plt.close()
#
#    fig = plt.figure(figsize=(12,4))
#    ax = fig.add_subplot(111)
#    ax.plot(np.array(resUser_HdF) - np.array(resSource_HdF), label='Res')
#    ax.plot(np.multiply(-1,pumpSource_hlQF), label='Pump')
#    ax.plot(np.array(tankUser_HdF) - np.array(tankSource_HdF), label='Tank')
#    ax.plot(np.multiply(-1,juncUser_HdF), label='Consumer')
#    ax.plot(np.multiply(-1,pipeUser_hlQF + valveUser_hlQF), label='Losses')
#    ax.set_xlabel('Time')
#    ax.set_ylabel('Energy (gpm*ft)')
#    ax.set_xlim([0,48])
#    ax.legend(loc=9,bbox_to_anchor=(0.5,-0.20),ncol=5)
#    fig.tight_layout()
#    if new == 'Reduc':
#        fig.savefig('S1EnergyBreakdown_'+net+'.png', format='png', dpi=300)
#    elif new =='Flat':
#        fig.savefig('S2EnergyBreakdown_'+net+'.png', format='png', dpi=300)
#    plt.close()
#
##     Results Summary Table - Excel
#    df_filename = "EnergyBreakdown_"+net+".xlsx"
#    df_energyS0 = pd.DataFrame({'aTime_hr':range(0,49), \
#                         'bReservoirs_S0 (gal/30min*ft)':resSource_Hd, 'cPumps_S0':pumpSource_hlQ, 'dTanksSource_S0':tankSource_Hd, \
#                         'eJunctions_S0':juncUser_Hd, 'fTanksUser_S0':tankUser_Hd, 'fbReservoirsFill_S0':resUser_Hd, \
#                         'gPipe Headloss_S0':pipeUser_hlQ, 'hValve Headloss_S0':valveUser_hlQ})
#
#    if new == 'Reduc':
#        df_energyS1 = pd.DataFrame({'aTime_hr':range(0,49), \
#                                 'bReservoirs_S1 (gal/30min*ft':resSource_HdF, 'cPumps_S1':pumpSource_hlQF, 'dTanksSource_S1':tankSource_HdF, \
#                                 'eJunctions_S1':juncUser_HdF, 'fTanksUser_S1':tankUser_HdF, 'fbReservoirsFill_S1':resUser_HdF, \
#                                 'gPipe Headloss_S1':pipeUser_hlQF, 'hValve Headloss_S1':valveUser_hlQF})
#        frames = [df_energyS0, df_energyS1]
#        df_excel = pd.concat(frames,axis=1)
#        df_excel.to_excel(df_filename, sheet_name='S0_S1', index=False)
#    elif new == 'Flat':
#        df_energyS2 = pd.DataFrame({'aTime_hr':range(0,49), \
#                                 'bReservoirs_S2 (gal/30min*ft)':resSource_HdF, 'cPumps_S2':pumpSource_hlQF, 'dTanksSource_S2':tankSource_HdF, \
#                                 'eJunctions_S2':juncUser_HdF, 'fTanksUser_S2':tankUser_HdF, 'fbReservoirsFill_S2':resUser_HdF,\
#                                 'gPipe Headloss_S2':pipeUser_hlQF, 'hValve Headloss_S2':valveUser_hlQF})
#        dfRead = pd.read_excel(df_filename)
#        frames = [df_energyS0, df_energyS2]
#        df_excel = pd.concat(frames,axis=1)
#        writer = pd.ExcelWriter(df_filename)
#        dfRead.to_excel(writer, sheet_name='S0_S1', index=False)
#        df_excel.to_excel(writer, sheet_name='S0_S2', index=False)
#        writer.save()

#------------------------------#
#-----PLOTTING RESULTS 2.0-----#
#------------------------------#
    # WORK IN PROGRESS

#    # ---- Scatterplots - Info Only ----
#
#    # S0 Base
#    nodeHL_list = nodeloss_list[:len(junctions)]
#    df = pd.DataFrame({'age':age24hr_list,'leaks':tot_bgLeaks, 'hl':nodeHL_list})
#
#    # for xlim, ylim only - setting at upper or lower 2.5% (similar to mid 95%)
#    ageLim_low = np.percentile(df.age, 2.5)
#    leakLim_high = np.percentile(df.leaks, 97.5)
#    hlLim_high = np.percentile(df.hl, 97.5)
#
#    # S1 Conservation
#    nodeHLF_list = nodelossF_list[:len(junctions)]
#    dfF = pd.DataFrame({'age':age24hrF_list,'leaks':tot_bgLeaksF, 'hl':nodeHLF_list})
#
#    # for xlim, ylim only - setting at upper or lower 2.5% (similar to mid 95%)
#    ageLimF_low = np.percentile(dfF.age, 2.5)
#    leakLimF_high = np.percentile(dfF.leaks, 97.5)
#    hlLimF_high = np.percentile(dfF.hl, 97.5)
#
#    # Plot: S0 Only
#    fig = plt.figure(figsize=(12,3))
#
#    ax = fig.add_subplot(131)
#    ax.scatter(df.age, df.leaks, s=2, color='C1')
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Water Loss (gpm)')
#    ax.set_xlim(xmin=int(ageLim_low))
#    ax.set_ylim(ymax=int(leakLim_high))
#
#    legend_elements = [Line2D([0], [0], marker='o', color='w', label='S0 Junction',
#                              markerfacecolor='C1', markersize=4)]
#    ax.legend(handles=legend_elements)
#
#    ax = fig.add_subplot(132)
#    ax.scatter(df.age, df.hl, s=2, color='C1')
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmin=int(ageLim_low))
#    ax.set_ylim(ymax=int(hlLim_high))
#
#    ax = fig.add_subplot(133)
#    ax.scatter(df.leaks, df.hl, s=2, color='C1')
#    ax.set_xlabel('Total Water Loss (gpm)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmax=int(leakLim_high))
#    ax.set_ylim(ymax=int(hlLim_high))
#
#    fig.tight_layout()
#    fig.savefig('S0Base_Scatter_95p.png', format='png', dpi=300)
#
#    # Plot: S1 Only
#    fig = plt.figure(figsize=(12,3))
#
#    ax = fig.add_subplot(131)
#    ax.scatter(dfF.age, dfF.leaks, s=2, marker='s')
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Water Loss (gpm)')
#    ax.set_xlim(xmin=int(ageLimF_low))
#    ax.set_ylim(ymax=int(leakLimF_high))
#
#    legend_elements = [Line2D([0], [0], marker='s', color='w', label='S1 Junction',
#                              markerfacecolor='C0', markersize=4)]
#    ax.legend(handles=legend_elements)
#
#    ax = fig.add_subplot(132)
#    ax.scatter(dfF.age, dfF.hl, s=2, marker='s')
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmin=int(ageLimF_low))
#    ax.set_ylim(ymax=int(hlLimF_high))
#
#    ax = fig.add_subplot(133)
#    ax.scatter(dfF.leaks, dfF.hl, s=2, marker='s')
#    ax.set_xlabel('Total Water Loss (gpm)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmax=int(leakLimF_high))
#    ax.set_ylim(ymax=int(hlLimF_high))
#
#    fig.tight_layout()
#    fig.savefig('S1Conservation_Scatter_95p.png', format='png', dpi=300)
#
#    # Plot: S0 & S1
#    fig = plt.figure(figsize=(12,3))
#
#    ax = fig.add_subplot(131)
#    ax.scatter(df.age, df.leaks, s=2, color='C1', marker='o', alpha=0.75)
#    ax.scatter(dfF.age, dfF.leaks, s=2, color='C0', marker='s', alpha=0.75)
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Water Loss (gpm)')
#    ax.set_xlim(xmin=int(min(ageLim_low,ageLimF_low)))
#    ax.set_ylim(ymax=int(max(leakLim_high,leakLimF_high)))
#
#    legend_elements = [Line2D([0], [0], marker='o', color='w', label='S0 Junction',
#                              markerfacecolor='C1', markersize=4), Line2D([0],
#                              [0], marker='s', color='w', label='S1 Junction',
#                              markerfacecolor='C0', markersize=4)]
#    ax.legend(handles=legend_elements)
#
#    ax = fig.add_subplot(132)
#    ax.scatter(df.age, df.hl, s=2, color='C1', marker='o', alpha=0.75)
#    ax.scatter(dfF.age, dfF.hl, s=2, color='C0', marker='s', alpha=0.75)
#    ax.set_xlabel('Max Age (hr)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmin=int(min(ageLim_low,ageLimF_low)))
#    ax.set_ylim(ymax=int(max(hlLim_high,hlLimF_high)))
#
#    ax = fig.add_subplot(133)
#    ax.scatter(df.leaks, df.hl, s=2, color='C1', marker='o', alpha=0.75)
#    ax.scatter(dfF.leaks, dfF.hl, s=2, color='C0', marker='s', alpha=0.75)
#    ax.set_xlabel('Total Water Loss (gpm)')
#    ax.set_ylabel('Total Head Loss (ft)')
#    ax.set_xlim(xmax=int(max(leakLim_high,leakLimF_high)))
#    ax.set_ylim(ymax=int(max(hlLim_high,hlLimF_high)))
#
#    fig.tight_layout()
#    fig.savefig('S0andS1_Scatter_95p.png', format='png', dpi=300)
#
#    # ---- American Flag Plots - Info Only ----
#    # S0 Base vs S1 Conservation
#    # Array = _list and F_list
#    df_age = pd.DataFrame({'S0':age24hr_list,'S1':age24hrF_list})
#    df_leak = pd.DataFrame({'S0':tot_bgLeaks,'S1':tot_bgLeaksF})
#    df_hl = pd.DataFrame({'S0':nodeloss_list,'S1':nodelossF_list})
#    df_flow = pd.DataFrame({'S0':peakflow_list,'S1':peakflowF_list})
#
#    fig = plt.figure(figsize=(12,16))
#
#    ax = fig.add_subplot(411)
#    for i in range(0, len(df_age)):
#        x = i*np.ones(2)
#        y = list(df_age.iloc[i,:])
#        ax.plot(x, y, color='C0')
#    ax.set_xlabel('Junctions')
#    ax.set_ylabel('Max Age (hr)')
#
#    ax = fig.add_subplot(412)
#    for i in range(0, len(df_leak)):
#        x = i*np.ones(2)
#        y = list(df_leak.iloc[i,:])
#        ax.plot(x, y, color='C0')
#    ax.set_xlabel('Junctions')
#    ax.set_ylabel('Tot Water Loss (gpm)')
#
#    ax = fig.add_subplot(413)
#    for i in range(0, len(df_hl)):
#        x = i*np.ones(2)
#        y = list(df_hl.iloc[i,:])
#        ax.plot(x, y, color='C0')
#    ax.set_xlabel('Nodes')
#    ax.set_ylabel('Tot Head Loss (ft)')
#
#    ax = fig.add_subplot(414)
#    for i in range(0, len(df_flow)):
#        x = i*np.ones(2)
#        y = list(df_flow.iloc[i,:])
#        ax.plot(x, y, color='C0')
#    ax.set_xlabel('Pipes')
#    ax.set_ylabel('Peak Flow (gpm)')
#
#    fig.tight_layout()
#    fig.savefig('S0andS1_AmericanFlagPlot.png', format='png', dpi=300)
#
#    # ---- Just the Data in Excel - Info Only ----
#    energyNodes = [node for node in list(n) if n[node].node_type==0]
#    df_filename = "Data_4_Rvwr1_Comments.xlsx"
#    df_junc = pd.DataFrame({'JunctionIndex': junctions,\
#                             '24hrAge_hr_S0':age24hr_list, '24hrAge_hr_S1':age24hrF_list, \
#                             'TotalWaterLoss_gpm_S0':tot_bgLeaks, 'TotalWaterLoss_gpm_S1':tot_bgLeaksF})
#    df_enNode = pd.DataFrame({'NodeIndex':energyNodes, \
#                             'TotalHeadLossByNodes_ft_S0':nodeloss_list, 'TotalHeadLossByNodes_ft_S1':nodelossF_list})
#    df_pipe = pd.DataFrame({'PipeIndex':pipes_list, \
#                             'PeakFlow_gpm_S0':peakflow_list, 'PeakFlow_gpm_S1':peakflowF_list})
#    frames = [df_junc, df_enNode, df_pipe]
#    df_excel = pd.concat(frames,axis=1)
#    df_excel.to_excel(df_filename, sheet_name='Sheet1', index=False)


#--------------------------#
#-----PLOTTING RESULTS-----#
#--------------------------#

    scenario = new

    #--------------------------------------#
    #-----PLOT: COMPARATIVE HISTOGRAMS-----#
    #--------------------------------------#

#    # Plot properties - general
#    fontsize = 16
#    if scenario == "Reduc":
#        yticks = np.arange(-.6,1.0,.2)
#        yticklabels = [0.6,0.4,0.2,0,0.2,0.4,0.6,0.8]
#    else:
#        yticks = np.arange(-.6,1.0,.2)
#        yticklabels = [0.6,0.4,0.2,0,0.2,0.4,0.6,0.8]
#
#    def zoom(x, percent):
#        # "Zoom" in on histogram
#        # absolute value
#        x = abs(x)
#        # find upper tail cutoff
#        cutoff = np.percentile(x,percent)
#        # cut (don't show) bins after that point in plot code
#        return cutoff
#
#    def compHisto(x, percent):
#        # Get values from histogram
#        # if don't want zoom, set percent = 100
#        bins = np.linspace(0,zoom(x,percent),int(jp.Sturges(abs(x))))
#        xP = [i for i in x if i >= 0]
#        xN = [abs(i) for i in x if i < 0]
#        weight = 1.0/len(x)
#        weights_xP = np.ones_like(xP) * weight
#        weights_xN = np.ones_like(xN) * weight
#        binVal_xP = plt.hist(xP, bins=bins, weights=weights_xP)
#        binVal_xN = plt.hist(xN, bins=bins, weights=weights_xN)
#        plt.close()
#
#        return bins, binVal_xP, binVal_xN
#
#    # Plot modified comparative histogram = a bar chart
#    alpha1 = 99 # cutoff so we have a zoomed-in histo, for wloss and age
#    alpha2 = 99 # for energy loss and peak flow
#
#    # Water Loss
#    x = delta_nLeak
#    bins, binVal_xP, binVal_xN = compHisto(x,alpha1)
#    fig, ax = plt.subplots(figsize=[5,5])
#    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
#    widthsBar = np.ediff1d(bins)[0]
#    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
#    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
#    ax.set_xlabel(r'$\Delta$ Water Loss (gpm)', fontsize=fontsize)
#    ax.set_ylabel('Frequency', fontsize=fontsize)
#    ax.set_xticks(bins[0:-1:2])
#    ax.set_xlim(bins[0],bins[-1])
#    ax.set_yticks(yticks)
#    ax.set_yticklabels(yticklabels)
#    ax.tick_params(axis='both', which='major', labelsize=fontsize)
#    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
#    plt.tight_layout()
#    fig.savefig(save_dir+scen+'-'+'WLossHisto_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#    plt.close(fig)
#
#    # Water Age
#    x = delta_age24hr
#    bins, binVal_xP, binVal_xN = compHisto(x, alpha1)
#    fig, ax = plt.subplots(figsize=[5,5])
#    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
#    widthsBar = np.ediff1d(bins)[0]
#    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
#    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
#    ax.set_xlabel(r'$\Delta$ Water Age (hr)', fontsize=fontsize)
#    ax.set_ylabel('Frequency', fontsize=fontsize)
#    ax.set_xticks(bins[0:-1:2])
#    ax.set_xlim(bins[0],bins[-1])
#    ax.set_yticks(yticks)
#    ax.set_yticklabels(yticklabels)
#    ax.tick_params(axis='both', which='major', labelsize=fontsize)
#    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f')) # or %d
#    plt.tight_layout()
#    fig.savefig(save_dir+scen+'-'+'AgeHisto_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#    plt.close(fig)
#
#    # Energy Loss
#    x = delta_nloss
#    bins, binVal_xP, binVal_xN = compHisto(x, alpha2)
#    fig, ax = plt.subplots(figsize=[5,5])
#    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
#    widthsBar = np.ediff1d(bins)[0]
#    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
#    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
#    ax.set_xlabel(r'$\Delta$ Energy Loss (ft)', fontsize=fontsize)
#    ax.set_ylabel('Frequency', fontsize=fontsize)
#    ax.set_xticks(bins[0:-1:2])
#    ax.set_xlim(bins[0],bins[-1])
#    ax.set_yticks(yticks)
#    ax.set_yticklabels(yticklabels)
#    ax.tick_params(axis='both', which='major', labelsize=fontsize)
#    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
#    plt.tight_layout()
#    fig.savefig(save_dir+scen+'-'+'EnergyHisto_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#    plt.close(fig)
#
#    # Peak Flow
#    x = delta_peakflow
#    bins, binVal_xP, binVal_xN = compHisto(x, alpha2)
#    fig, ax = plt.subplots(figsize=[5,5])
#    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
#    widthsBar = np.ediff1d(bins)[0]
#    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
#    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
#    ax.set_xlabel(r'$\Delta$ Peak Flow (gpm)', fontsize=fontsize)
#    ax.set_ylabel('Frequency', fontsize=fontsize)
#    ax.set_xticks(bins[0:-1:2])
#    ax.set_xlim(bins[0],bins[-1])
#    ax.set_yticks(yticks)
#    ax.set_yticklabels(yticklabels)
#    ax.tick_params(axis='both', which='major', labelsize=fontsize)
#    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
#    plt.tight_layout()
#    fig.savefig(save_dir+scen+'-'+'PkFlowHisto_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#    plt.close(fig)

    #----------------------------------#
    #-----PROPERTIES FOR MORE PLOTS----#
    #----------------------------------#

    for metric in metrics:
        if metric == 'flow':
            features = 'Pipes'
            prop = flow
            unit = ' (LPS) '
            label = 'Flow'
            label2 = 'Peak Flow'
            ascend = False
            deltaX = delta_peakflow
        elif metric == 'headloss':
            features = 'Nodes'
            prop = hl
            unit = ' (ft) '
            label = 'Energy Loss'
            ascend = True
            deltaX = delta_nloss
        elif metric == 'pressure':
            features = 'Junctions'
            prop = p
            unit = ' (m) '
            label = 'Pressure'
            ascend = True
            deltaX = delta_allPressRes
        elif metric == 'waterloss':
            features = 'Junctions'
            unit = ' (gpd) '
            label = 'Water Loss'
            ascend = True
            deltaX = delta_nLeak
        else:
            features = 'Junctions'
            prop = q
            unit = ' (hr) '
            label = 'Water Age'
            ascend = True
            deltaX = delta_age24hr

        ylabel = features
        xlabel = 'Time (hr)'

        #--------------------------------------------#
        #-----CREATE DATAFRAMES OF METRIC VALUES-----#
        #--------------------------------------------#

        # create dataframes for NOMINAL and NEW scenarios
        if metric == 'headloss':
            propArray = nodeLossAll(n,l,ts_index)
            propArrayF = nodeLossAll(nF,lF,ts_indexF)
            dfStrip = pd.DataFrame(propArray)
            dfStrip.columns = range(0,len(ts_index))
            dfStripF = pd.DataFrame(propArrayF)
            dfStripF.columns = range(0,len(ts_indexF))
        elif metric == 'waterloss':
            propArray = np.array(tot_bgLeaks)
            propArrayF = np.array(tot_bgLeaksF)
            dfStrip = pd.DataFrame(propArray)
            dfStripF = pd.DataFrame(propArrayF)
        else:
            dfStrip, propArray = jp.df4MatrixPlot_all(features,prop,ts_index,n,l,metric,'Nom',net)
            dfStripF, propArrayF = jp.df4MatrixPlot_all(features,prop,ts_indexF,nF,lF,metric,scenario,net)
        # for water age, only want last timestep!
        if metric == 'age':
            dfStrip = dfStrip.iloc[:,-1]
            dfStrip = pd.concat([dfStrip,dfStrip],axis=1)
            dfStripF = dfStripF.iloc[:,-1]
            dfStripF = pd.concat([dfStripF,dfStripF],axis=1)
            propArrayF = propArrayF[-1]
        elif metric == 'flow':
            dfStrip = abs(dfStrip)
            propArray = abs(np.array(propArray))
            dfStripF = abs(dfStripF)
            propArrayF = abs(np.array(propArrayF))
        cut1, cut2, dataNew = jp.midPercentile(propArrayF,percent,metric)

        # create dataframe for delta (NEW - NOMINAL)
        dfStripF.columns = dfStrip.columns
        dfDiff = dfStripF.subtract(dfStrip)

        # reorder features by delta
        dfDiff["Med"] = dfDiff.median(axis=1)
        dfDiff = dfDiff.sort_values(by="Med", ascending=ascend)
        df = dfDiff.reset_index() # don't drop prev index bc it's feature index-1
        dfDiff = df[df.columns.difference(['index','Med'])]

        #-----------------------------#
        #-----PLOT: HEAT MATRICES-----#
        #-----------------------------#
#        if metric != 'waterloss' and metric != 'age':
#            # Colored heat maps/matrices of values for ALL features in network, at ALL times, disregarding outliers
#            fontsize = 22
#            sns.set(font_scale=1.8)
#            data = dfDiff
#            data.columns = np.arange(0,24.5,0.5)
#            cut1, cut2, dataNew = jp.midPercentile(dfDiff,percent,metric)
#            fig, ax = plt.subplots(figsize=[7,8])
#            sns.heatmap(data, ax=ax, vmin=cut1, vmax=cut2, center=0, cmap='RdBu_r',\
#                        yticklabels=False, xticklabels=12, \
#                        cbar_kws={'label':r'$\Delta$ '+label+unit, 'shrink':0.75})
#            ax.tick_params(axis='both', which='major', labelsize=fontsize)
#            plt.ylabel(ylabel, fontsize=fontsize)
#            plt.xlabel(xlabel, fontsize=fontsize)
#            plt.tight_layout()
#            fig.savefig(save_dir+scen+'-HeatMatrix'+metric+'_Net'+netDict[net]+'.png',format='png', dpi=100)
#            plt.close(fig)

        #----------------------------#
        #-----AGGREGATING VALUES-----#
        #----------------------------#

        if metric != 'age':
            # Get flow 49 flow data points for each node/pipe
            # Add each of these lists to a list = data
            data = dfDiff

            #-------------------------------------#
            #-----PLOT: MULTI BOXPLOT CONCEPT-----#
            #-------------------------------------#

#            # Showing all values across time for each feature
#            # multiple boxplots on one Axes
#            fig, ax = plt.subplots()
#            medianprops = dict(linestyle=None, linewidth=0)
#            boxprops = dict(color='C7')
#            whiskerprops = dict(color='C7')
#            test = ax.boxplot(data, showcaps=False, showfliers=False, \
#                              showmeans=False, medianprops=medianprops, \
#                              boxprops=boxprops, whiskerprops=whiskerprops)
#            medians = [round(test['medians'][i].get_ydata()[0],2) for i in range(0,len(dfDiff))]
#            #upWhiskers = [test['whiskers'][i].get_ydata()[0] for i in range(0,len(dfDiff))]
#            #lowWhiskers = [test['whiskers'][i].get_ydata()[1] for i in range(0,len(dfDiff))]
#            mediansPos = [v for v in medians if v > 0]
#            mediansNeg = [v for v in medians if v <= 0]
#
#            imed = 0
#            for v in medians:
#                # last time v > 0 is split between positiive and negative values
#                if v > 0:
#                    stop = imed
#                imed += 1
#            ax.plot(range(0,stop+1),mediansPos,'C3-')
#            ax.plot(range(stop+1,len(medians)),mediansNeg,'C0-')
#            #ax.plot(upWhiskers,'C0-.')
#            #ax.plot(upWhiskers,'C2-.')
#            ax.set_title(net+' '+metric+' '+scenario+'-Nom TimeAggregate')
#            ax.set_xlabel('Sorted by median delta')
#            ax.set_ylabel(metric)
#            ax.get_xaxis().set_ticklabels([])
#            ax.get_xaxis().set_ticks([])
#            fig.savefig(save_dir+scen+'-MultiBP'+metric+'_Net'+netDict[net]+'.pdf',format='pdf', dpi=100)
#            plt.close(fig)

            #--------------------------------#
            #-----PLOT: "AMERICAN FLAGS"-----#
            #--------------------------------#

#            # Showing range of all values in network by time
#            # transpose dfDiff
#            dfDiff_T = dfDiff.transpose()
#            dfDiff_T = dfDiff_T.reset_index()
#            dfDiff_T = dfDiff_T.drop('index',axis=1)
#
#            # Conversions to SI units for paper v2
#            if metric == "pressure":
#                # 1 psi = 0.70324961490205 m
#                dfDiff_T = dfDiff_T.multiply(0.70324961490205)
#            elif metric == "flow":
#                # 1 gpm = 0.0630902	lps
#                dfDiff_T = dfDiff_T.multiply(0.0630902)
#
#            fsize=20 # for trim + 0.48 scale
#            fig, ax = plt.subplots()
#            plt.axhline(0, color='C7', linewidth=0.85, alpha = 0.75)
#            for i in range(0, dfDiff_T.shape[0]):
#                subset = dfDiff_T.iloc[i,:]
#                cut1, cut2, subsetNew = jp.midPercentile(subset,95,metric)
#                mid95 = [v for v in subset if (v>=cut1 and v<=cut2)]
#                tails = [v for v in subset if v not in mid95]
#                ax.plot(i*np.ones(len(tails)), tails, 'C7',markersize=0, alpha=0.75)
#                ax.plot(i*np.ones(len(mid95)), mid95, 'C0',markersize=0,linewidth=3)
#            ax.set_xlabel('Time (hr)',fontsize=fsize)
#            ax.set_ylabel(r'$\Delta$ '+metric+unit, fontsize=fsize)
#            ax.set_xticks(range(0,49,12))
#            ax.set_xticklabels(range(0,25,6), fontsize=fsize)
#            ax.tick_params(axis='both', which='major', labelsize=fsize)
##            ax.set_yticklabels(ax.get_yticklabels(), fontsize=fsize)
#            plt.tight_layout()
##            fig.savefig(save_dir+scen+'-Feat'+metric+'_Net'+netDict[net]+'.pdf',format='pdf', dpi=100)
#            fig.savefig(save_dir+'S'+scen+'_'+metric+'_Net'+netDict[net]+'.pdf',format='pdf', dpi=100)
#            plt.close(fig)

        #-------------------------------#
        #-----COLORED GRAPHS SET-UP-----#
        #-------------------------------#

        # assign weights used for node or edge sizes
        dfWeights = pd.DataFrame(df['index'])
        if metric == 'age':
            medians = dfDiff.iloc[:,0]
        elif metric == 'flow':
            medians = delta_peakflow
        else:
            # for 'headloss' and 'waterloss'
            medians = dfDiff.sum(axis=1)
            medians = medians * 30 #loss per day
        dfWeights['Med'] = medians
        dfWeights = dfWeights.sort_values(by='index')
        dfWeights = dfWeights.reset_index(drop=True)
        meds = dfWeights['Med']
        weightValues = []
        if features == 'Pipes':
            i = 0
            for edge in list(l):
                if l[edge].link_type==1:
                    weightValues.append(meds[i])
                    i += 1
                else:
                    weightValues.append(0)
        elif features == 'Junctions':
            i = 0
            for node in list(n):
                if node in junctions:
                    weightValues.append(meds[i])
                    i += 1
                else:
                    weightValues.append(0)
        else:
            energyNodes = [node for node in list(n) if n[node].node_type==0]
            i = 0
            for node in list(n):
                if node in energyNodes:
                    weightValues.append(meds[i])
                    i += 1
                else:
                    weightValues.append(0)

        # get components to draw network graph
        G, pos, colored, weights = coloredFeatures(net, path, weightValues, features)

        # cut outliers
        cut1, cut2, dataNew = jp.midPercentile2(weightValues,percent,"")

        # source nodes
        sources = [node for node in list(n) if n[node].node_type!=0]

        #---------------------------------------#
        #-----PLOT: NETWORK GRAPHS SKELETON-----#
        #---------------------------------------#

#        fig, ax = plt.subplots(figsize=[6,6])
#        plt.axis('equal')
#
#        # network skeleton
#        nx.draw(G,pos, node_size=0, node_color='k', edge_color='C7', width=0.5)
#
#        plt.tight_layout()
#        fig.savefig(save_dir+net+'network.pdf', format='pdf', dpi=300)
#        plt.close(fig)

        #--------------------------------------#
        #-----PLOT: COLORED NETWORK GRAPHS-----#
        #--------------------------------------#

#        # for COLOR MAPPING NODES
#        if metric != 'flow':
#
#            fig, ax = plt.subplots(figsize=[6,6])
#            plt.axis('equal')
#
#            # network skeleton
#            nx.draw(G,pos, node_size=0, node_color='k', edge_color='C7', width=0.5)
#
#            # for sizing nodes by weight
#            if features != 'Pipes':
#                nodesPos = []
#                nodesNeg = []
#                nodesPosThick = []
#                nodesNegThick = []
#                for feat in colored:
#                    # positive and in upper 2.5%
#                    if G.nodes[feat]['weight'] > 0 and G.nodes[feat]['weight'] >= cut1:
#                        nodesPosThick.append(feat)
#                    # just positive
#                    elif G.nodes[feat]['weight'] > 0:
#                        nodesPos.append(feat)
#                    # negative or zero and in lower 2.5%
#                    elif G.nodes[feat]['weight'] <= 0 and G.nodes[feat]['weight'] <= cut2:
#                        nodesNegThick.append(feat)
#                    # just negative
#                    elif G.nodes[feat]['weight'] <= 0:
#                        nodesNeg.append(feat)
#                # negative nodes, normal size
#                nx.draw(G,pos, nodelist=nodesNeg, edgelist=[], width=0.5, \
#                        node_size=2, node_color='C0', alpha=0.75)
#                # positive nodes, normal size
#                nx.draw(G,pos, nodelist=nodesPos, edgelist=[], width=0.5, \
#                        node_size=2, node_color='C3', alpha=0.75)
#                # negative nodes, larger
#                nx.draw(G,pos, nodelist=nodesNegThick, edgelist=[], width=0.5, \
#                        node_size=50, node_color='C0', alpha=0.75, label=r'$x\leq$'+str(round(cut2,1))+unit)
#                # positive nodes, larger
#                nx.draw(G,pos, nodelist=nodesPosThick, edgelist=[], width=0.5, \
#                        node_size=50, node_color='C3', alpha=0.75, label=str(round(cut1,1))+r'$x\geq$'+unit)
#
#            # source nodes
#            nx.draw(G,pos, nodelist=sources, edgelist=[], width=0.5, \
#                    node_size=35, node_color='k', node_shape='s', alpha=0.75)
#
#            ax.legend(frameon=False,loc=9,bbox_to_anchor=(0.5,0.05),ncol=2)
#            plt.tight_layout()
#            fig.savefig(save_dir+scen+'-ColoredGraph'+metric+'_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#            plt.close(fig)
#
#        # for COLOR MAPPING EDGES
#        else:
#            # for 'flow' only:
#            fig, ax = plt.subplots(figsize=[6,6])
#            plt.axis('equal')
#            # base
#            nx.draw(G,pos, node_size=0, node_color='k', edge_color='C7', width=0.5)
#
#            # for sizing edges by weights:
#            Pos = []
#            Neg = []
#            PosThick = []
#            NegThick = []
#            for feat in colored:
#                # positive and in upper 2.5%
#                if G.edges[feat]['weight'] > 0 and G.edges[feat]['weight'] >= cut1:
#                    PosThick.append(feat)
#                # just positive
#                elif G.edges[feat]['weight'] > 0:
#                    Pos.append(feat)
#                # negative or zero and in lower 2.5%
#                elif G.edges[feat]['weight'] <= 0 and G.edges[feat]['weight'] <= cut2:
#                    NegThick.append(feat)
#                # just negative
#                elif G.edges[feat]['weight'] <= 0:
#                    Neg.append(feat)
#            # negative, normal size
#            nx.draw(G,pos, edgelist=Neg, width=0.5, \
#                    node_size=0, edge_color='C0', alpha=0.75)
#            # positive, normal size
#            nx.draw(G,pos, edgelist=Pos, width=0.5, \
#                    node_size=0, edge_color='C3', alpha=0.75)
#            # negative, larger
#            nx.draw(G,pos, edgelist=NegThick, width=4, \
#                    node_size=0, edge_color='C0', alpha=0.75)
#            # positive, larger
#            nx.draw(G,pos, edgelist=PosThick, width=4, \
#                    node_size=0, edge_color='C3', alpha=0.75)
#
#            # source nodes
#            nx.draw(G,pos, nodelist=sources, edgelist=[], width=0.5, \
#                    node_size=35, node_color='k', node_shape='s', alpha=0.75)
#
#            legend_elements = [Line2D([0],[0],color='C0',lw=4,label=r'$x\leq$'+str(round(cut2,1))+unit), \
#                               Line2D([0],[0],color='C3',lw=4,label=str(round(cut1,1))+r'$x\geq$'+unit)]
#            ax.legend(handles=legend_elements, \
#                      frameon=False,loc=9,bbox_to_anchor=(0.5,0.00),ncol=2)
#            plt.tight_layout()
#            fig.savefig(save_dir+scen+'-ColoredGraph'+metric+'_Net'+netDict[net]+'.pdf', format='pdf', dpi=300)
#            plt.close(fig)

##--------------------------------#
##-----WRITE RESULTS TO EXCEL-----#
##--------------------------------#
## INSTRUCTIONS: 1) First, run script with new='Reduc', and all networks listed
##               2) Second, run script with new='Flat', and all networks listed
#
#df_filename = 'All_Net_Info_95P_DDA_Revised.xlsx'
## Part 1: general network characteristics
#dfNet = pd.DataFrame({'Network': networks, 'No. Nodes': nodeNoList, \
#                'No. Pipes': pipeNoList, \
#                'Tot Pipe Length (Kft)': lengthList, \
#                'Tot Base Demand (MGD)': totbdList, \
#        })
## Part 2: results, nominal pattern
#dfBase = pd.DataFrame({'Leakage (MGD)-Base': np.array(allnLeak)/10**6, \
#                    'Age (hr)-Base': allAge, \
#                    'Peak Flow (gpm)-Base': apfList, \
#                    'Energy Loss (k-ft)-Base': np.array(allLoss)/1000, \
#        })
## Part 3: results, new scenario pattern
#if new == 'Reduc':
#    dfReduc = pd.DataFrame({'Leakage (MGD)-' +new: np.array(allnLeakF)/10**6, \
#                        'Age (hr)-' +new: allAgeF, \
#                        'Peak Flow (gpm)-' +new: apfListF, \
#                        'Energy Loss (k-ft)-' +new: np.array(allLossF)/1000, \
#        })
#    dfPct_Reduc = pd.DataFrame({'Leakage (%)-' +new: pctDiffwLoss, \
#                        'Age (%)-' +new: pctDiffwAge, \
#                        'Peak Flow (%)-' +new: pctDiffpFlow, \
#                        'Energy Loss (%)-' +new: pctDiffeLoss, \
#                        })
#    frames = [dfNet,dfBase,dfReduc,dfPct_Reduc]
#else:
#    dfFlat = pd.DataFrame({'Leakage (MGD)-' +new: np.array(allnLeakF)/10**6, \
#                        'Age (hr)-' +new: allAgeF, \
#                        'Peak Flow (gpm)-' +new: apfListF, \
#                        'Energy Loss (k-ft)-' +new: np.array(allLossF)/1000, \
#    })
#    dfPct_Flat = pd.DataFrame({'Leakage (%)-' +new: pctDiffwLoss, \
#                        'Age (%)-' +new: pctDiffwAge, \
#                        'Peak Flow (%)-' +new: pctDiffpFlow, \
#                        'Energy Loss (%)-' +new: pctDiffeLoss, \
#                        })
#
#    dfRead = pd.read_excel(df_filename)
#    frames = [dfRead,dfFlat,dfPct_Flat]
#
## concatenate the parts
#df = pd.concat(frames, axis=1)
## write to excel
#df.to_excel(df_filename, sheet_name='Sheet1',index=False)