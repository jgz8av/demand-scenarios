"""
for Project Demand Management: Batch create Network Graph Plots from .inp files
Author: Janice Zhuang
Python 2.7
"""

import networkx as nx
from epanettools.epanettools import EPANetSimulation

def coloredFeatures(net, path, weightValues, features):

    # get coordinates from .inp
    with open(path, 'r') as f:
        i = 0
        for line in iter(f):
            split = line.split()
            if split and split[0] == "[COORDINATES]":
                coord_start = i+2
            if split and split[0] == "[VERTICES]":
                coord_end = i-2
            i += 1

    with open(path, 'r') as f:
        nodes = [] # node
        nodex = [] # node x-coord
        nodey = [] # node y-coord
        coord = {} # node position dictionary
        nlabels = {} # node labels dictionary
        elabels = {} # edge labels dictionary
        i = 0
        ni = 1
        for line in iter(f):
            split = line.split()
            # getting coordinates of nodes
            if coord_start <= i <= coord_end:
                nodes.append(split[0])
                nodex.append(float(split[1]))
                nodey.append(float(split[2]))
                coord[ni] = (float(split[1]),float(split[2]))
                nlabels[ni] = split[0]
                ni += 1
            i += 1

    # get link connections via EPANET
    es = EPANetSimulation(path)
    n = es.network.nodes
    l = es.network.links
    links = {}
    elabels = {}
    li = 1
    for link in list(l):
        links[li] = (l[link].start.index, l[link].end.index)
        elabels[li] = link
        li += 1

    #----DRAWING UNDIRECTED GRAPH
    G = nx.Graph()

    # add nodes to graph
    i = 1
    for node in nodes:
        G.add_node(i)
        i += 1

    # add coordinate positions to nodes
    pos=coord
    for node, p in pos.iteritems():
        G.node[node]['pos'] = p
#    print G.nodes() # check nodes added correctly

    # add edges' start & end to graph
    i = 1
    for edge in links.keys():
        G.add_edge(links[i][0], links[i][1])
        i += 1
#    print G.edges() # check edges added correctly

    # add weights to edges
    if features == 'Pipes':
        i = 0
        for u,v,d in G.edges(data=True):
            d['weight'] = weightValues[i]
            i += 1
        colored,weights = zip(*nx.get_edge_attributes(G,'weight').items())
    # add weights to nodes
    else:
        i = 0
        for p,d in G.nodes(data=True):
            d['weight'] = weightValues[i]
            i += 1
        colored,weights = zip(*nx.get_node_attributes(G,'weight').items())
    return G, pos, colored, weights