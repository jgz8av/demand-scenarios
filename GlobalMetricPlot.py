"""
For demand-scenarios project:
    creating global metric plot
Author: Janice Zhuang
Python 2.7
"""

import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import parallel_coordinates
#import matplotlib.lines as mlines
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import seaborn as sns

#----FUNCTIONS
def normalize(x):
    # normalize data 0-1 range
    # x = list of all values
    # xi = value of interest to normalize
    # returns normalized list of values
    zList = []
    for xi in x:
        z = (xi - min(x)) / float((max(x) - min(x)))
        zList.append(z)
    return zList

def normalize_r(x):
    # REVERSE normalize data 1-0 range
    # x = list of all values
    # xi = value of interest to normalize
    # returns REVERSE normalized list of values
    zList = []
    for xi in x:
        z = (xi - max(x)) / float((min(x) - max(x)))
        zList.append(z)
    return zList

def greaterLess(x):
    # determine if new value is greater or less than base value
    # x = normalize([base, cons, shift]) so that cons and shift never = base
    binary = []
    r=3
    for xi in x[1::]:
        if round(xi,r) < round(x[0],r):
            binary.append(1)
        elif round(xi,r) == round(x[0],r):
            binary.append(0)
        else:
            binary.append(-1)
    return (binary[0],binary[1])

#----READ FILE
save_dir = 'results/figures/'
# file contains organized results from all networks
df_filename = 'results/All-Net_Info_95P4_v2.xlsx'
df = pd.read_excel(df_filename, sheet_name='Sheet1')
#print df.columns.values.tolist()
df = df.set_index('Network')
dfT = df.transpose()

patterns = ['S0','S1']

##----PLOTTING SCENARIO COMPARISON PLOTS FOR EACH NETWORK
#networks = df.index.tolist()
#cons_binary = []
#shift_binary = []
## 1 plot for ea. network
#for net in networks:
#    # get metric values and normalize
#    wLoss = dfT.loc[['Leakage (MGD)-Base','Leakage (MGD)-Reduc','Leakage (MGD)-Flat'],net]
#    wLoss = normalize_r(wLoss) # so that 1 is the BEST (lowest val) and 0 is the worse (highest val)
#    wLoss_binary = greaterLess(wLoss)
#
#    wAge = dfT.loc[['Age (hr)-Base','Age (hr)-Reduc','Age (hr)-Flat'],net]
#    wAge = normalize_r(wAge)
#    wAge_binary = greaterLess(wAge)
#
#    eLoss = dfT.loc[['Energy Loss (k-ft)-Base','Energy Loss (k-ft)-Reduc','Energy Loss (k-ft)-Flat'],net]
#    eLoss = normalize_r(eLoss)
#    eLoss_binary = greaterLess(eLoss)
#
#    pFlow = dfT.loc[['Peak Flow (gpm)-Base','Peak Flow (gpm)-Reduc','Peak Flow (gpm)-Flat'],net]
#    pFlow = normalize_r(pFlow)
#    pFlow_binary = greaterLess(pFlow)
#
#    # for plotting all
#    cons, shift = zip(*[wLoss_binary, wAge_binary, eLoss_binary, pFlow_binary])
#    cons_binary.append(list(cons))
#    shift_binary.append(list(shift))
#
#    # create df
#    metrics = [wLoss, wAge, eLoss, pFlow]
#    metrics_df = pd.DataFrame(metrics, columns=patterns)
#    metrics_df.index = ['Water Loss', 'Water Age','Energy Loss', 'Peak Flow']
#    metrics_df = metrics_df.transpose()
#    metrics_df['Pattern'] = patterns
#
#    # sizing for latex
#    if net =='ky4': # benchmark
#        fsize = 12
#        bottom = 0.18
#        anchor = -0.12
#    else:
#        fsize = 18
#        bottom = 0.24
#        anchor = -0.16
#
#    # plot parallel coord plots for scenario comparison for each network
#    fig, ax = plt.subplots(figsize=[6,4])
##    # black and white plots:
##    parallel_coordinates(metrics_df.iloc[[0]], 'Pattern', color=['grey'], axvlines=False, \
##                         linestyle='-', lw=2.5)
##    parallel_coordinates(metrics_df.iloc[[1]], 'Pattern', color=['k'], axvlines=False, \
##                     linestyle='--', lw=2.5)
##    parallel_coordinates(metrics_df.iloc[[2]], 'Pattern', color=['k'], axvlines=False, \
##                 linestyle='-', lw=2.5)
#    # color plots:
#    parallel_coordinates(metrics_df.iloc[[0]], 'Pattern', color=['grey'], axvlines=False, \
#                         linestyle='-', lw=2.5)
#    parallel_coordinates(metrics_df.iloc[[1]], 'Pattern', color=['C0'], axvlines=False, \
#                     linestyle='--', lw=2.5)
#    parallel_coordinates(metrics_df.iloc[[2]], 'Pattern', color=['yellowgreen'], axvlines=False, \
#                 linestyle='-', lw=2.5)
#    ax.set_yticks([0,0.5,1.0])
#    ax.set_xticklabels(['Water\nLoss', 'Water\nAge','Energy\nLoss', 'Peak\nFlow'], fontsize=fsize)
#    ax.tick_params(axis='both', which='major', labelsize=fsize)
#    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
#    # adjust grid and frame
#    ax.grid(color='grey',linestyle='-',lw=0.5,alpha=0.75)
#    ax.yaxis.grid(False)
#    [i.set_linewidth(0.5) for i in ax.spines.itervalues()]
#    # remove pandas legend
#    plt.gca().legend_.remove()
#    # add new legend
#    plt.legend(patterns, \
#               frameon=False,loc=9,bbox_to_anchor=(0.5,anchor),ncol=3,fontsize=fsize)
#    fig.tight_layout()
#    fig.subplots_adjust(bottom=bottom)
#    fig.savefig(save_dir+'ParCoord_Net'+str((networks.index(net)+1))+'.pdf',format='pdf', dpi=300)
#    plt.close(fig)

#----PLOTTING GLOBAL METRIC PLOT, contd
# note, these were manually converted to -1/0/+1 in excel. \
# But np.sign would do this.
pcts_df1 = pd.read_excel(df_filename, sheet_name='Sheet3', usecols=range(2,7)) # color ramp version
#pcts_df1 = pd.read_excel(df_filename, sheet_name='Sheet4', usecols=range(2,7)) # binary colors version

# rename networks from [u'ky3', u'ky5', u'ky6', u'ky7', u'ky1', u'ky8', u'ky4']
networks = ['Net1','Net2','Net3','Net4','Net5','Net6','Net7']
fsize = 20 # for 0.47 scale
#sns.set(font_scale=1)

pi = 0
for binary in [pcts_df1]:
    binary_df = binary
#    binary_df.columns = ['Water Loss', 'Water Age', 'Energy Input', 'Energy Loss', 'Peak Flow']
    binary_df.columns = ['Iw', 'Ia', 'If', 'Ie-input', 'Ie-loss']
    binary_df['Networks'] = networks

    # custom annotations
    annotations = np.empty((7,5), dtype=str)
    for index,value in np.ndenumerate(annotations):
        i = int(index[0])
        j = int(index[1])
        if binary.iloc[i,j] > 0:
            annotations[i,j] = '+'
        elif binary.iloc[i,j] < 0:
            annotations[i,j] = '-'
        else:
            annotations[i,j] = '0'

    # plot global metric plot
    fig2, ax2 = plt.subplots()
    binary_df = binary_df.drop(['Networks'], axis=1)
#    cmap = sns.xkcd_palette(['black', 'white', 'grey']) # black/white
#    cmap = ['C3', 'white', 'C0'] # match red/blue of other plots
#    cmap = ['C3', 'white', '#43a2ca'] # different tones for black/white version
    cmap = 'coolwarm_r'
    g = sns.heatmap(binary_df, cmap=cmap, annot=annotations, \
                    annot_kws={"size": fsize-4}, fmt = '', mask=annotations=='0', \
                    cbar=False, alpha=1, center=0, vmin=-100, vmax=100) # color ramp version
#    g = sns.heatmap(binary_df, cmap=cmap, annot=annotations, \
#                annot_kws={"size": fsize-4}, fmt = '', mask=annotations=='0', \
#                cbar=False, alpha=1, center=0) # binary colors version
    g.set_yticklabels(networks, rotation=0, fontsize=fsize)
#    g.set_xticklabels(['Water\nLoss', 'Water\nAge','Energy\nLoss', 'Peak\nFlow'], fontsize=fsize)
    g.set_xticklabels([r'$I_w$', r'$I_a$', r'$I_f$', r'$I_{e-input}$', r'$I_{e-loss}$'], fontsize=fsize)
    plt.tight_layout()
    fig2.savefig(save_dir+str(pi+1)+'-heatmap.png',format='png', dpi=300)
    pi += 1