"""
NOTE: I CHANGED THE NEW PATTERN TO nF and lF

Demand Management Project
Author: Janice Zhuang
Python 2.7
"""
import tempfile, os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
#import matplotlib.mlab as mlab
from matplotlib.ticker import FormatStrFormatter, ScalarFormatter
import plotly
import seaborn as sns
import statsmodels.api as sm
#import plotly.figure_factory as ff
#import networkx as nx
import pandas as pd
#from pandas.plotting import scatter_matrix
#from scipy import stats
#from scipy.interpolate import splrep, splprep, splev
#import statsmodels.api as sm
#from scipy.stats.kde import gaussian_kde
#from pprint import pprint
from epanettools.epanettools import EPANetSimulation, Node, Link, Network, \
                                    Nodes, Links, Patterns, Pattern, \
                                    Controls, Control
import janplots as jp #Janice's own functions
import networkx as nx
from PlotNetwork import coloredEdges


#-----INPUTS----------------------------------------------------------------
#metrics = ['flow', 'headloss', 'pressure', 'age']
#metrics = ['flow', 'headloss', 'pressure']
#metrics = ['waterloss', 'headloss', 'pressure', 'age', 'flow']
#metrics = ['headloss', 'pressure', 'flow']
#metrics = ['flow', 'pressure']
metrics = ['pressure']

networks = ['ky1', 'ky3', 'ky4', 'ky5', 'ky6', 'ky7', 'ky8']
#networks = ['ky1', 'ky3', 'ky5', 'ky6', 'ky7', 'ky8']
#networks = ['ky4']
#networks = ['ky8', 'ky4']

# scenario
new = 'Reduc'
#new = 'Flat'

# percentile
percent = 95

#---GENERAL NETWORK SETUP---------------------------------------------------
# TIME: 24 hour sim, 1 hour pattern time steps, 30 min reporting time step
hr = 1
nt = 24/hr
hstep = 3600/2  # 30 min hydraulic reporting time step
qstep = 3600/12 # 5 min quality reporting time step

# Network property variables
d = Node.value_type['EN_DEMAND']
p = Node.value_type['EN_PRESSURE']
hl = Link.value_type['EN_HEADLOSS']
e = Link.value_type['EN_ENERGY']
b = Node.value_type['EN_BASEDEMAND']
leng = Link.value_type['EN_LENGTH']
flow = Link.value_type['EN_FLOW']
q = Node.value_type['EN_QUALITY']
diam = Link.value_type['EN_DIAMETER']

#---FUNCTIONS---------------------------------------------------------------

# depends on epanet properties above
def getTimeSteps(tsteps):
    # get correct time steps
    tsteps_iter = range(0, len(tsteps))
    ts_index = []   # length should be divisible by total extended time pd (hrs)
    i = 0   # (hour 0 to hour 24) = 49 elements for 1-hour pat, 24-hr pd
    tempsum = 0
    for index in tsteps_iter:
        # include 1st & last index
        if i==tsteps_iter[0] or i==tsteps_iter[-1]:
            ts_index.append(i)
        # include 1800L index
        elif tsteps[i]==hstep:
            ts_index.append(i)
        # if partial step, loop through new list to find full steps
        elif tempsum + tsteps[i] == 1800:
            ts_index.append(i)
            tempsum = 0
        else:
            tempsum += tsteps[i]
        i += 1
    return ts_index

def getLinkValues(link,prop,ts_index,l):
    allValues = l[link].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

def getNodeValues(node,prop,ts_index,n):
    allValues = n[node].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    return correctValues

def getLinkValuesMid(link,prop,ts_index,l,percent):
    allValues = l[link].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    cut1, cut2, correctValues = jp.midPercentile(correctValues,percent,"")
    return correctValues

def getNodeValuesMid(node,prop,ts_index,n,percent):
    allValues = n[node].results[prop]
    correctValues = [allValues[i] for i in ts_index]
    cut1, cut2, correctValues = jp.midPercentile(correctValues,percent,"")
    return correctValues

#def getTrapArea(results,timestep):
#    series = np.array(results)
#    total = 0
#    i = 0
#    for value in series:
#        if i != len(series)-1:
#            area = timestep*(value + series[i+1])/2
#        total += area
#        i += 1
#    return total

def nodeLossAll(n,l,ts_index):
    # for time agg and node agg plots
    energyNodes = [node for node in list(n) if n[node].node_type==0]
    allNodeLosses = []
    for node in energyNodes:
        # list for pipes that connect
        pipes_connect = [i.index for i in n[node].links \
                         if l[i.index].link_type==1]
        # list for pipes that connect to a "specialty node"
        pipes_special = [pipe for pipe in pipes_connect \
                         if (l[pipe].start.index not in energyNodes) \
                         or (l[pipe].end.index not in energyNodes)]
        pipeLosses_list = [] # length of pipes_connect
        for pipe in pipes_connect:
            pipeLosses = np.array(getLinkValues(pipe,hl,ts_index,l)) # length of ts_index
            if pipe in pipes_special:
                pipeLosses = pipeLosses
            else:
                pipeLosses = 0.5*pipeLosses
            pipeLosses_list.append(pipeLosses)
        pipeLosses_list = np.array(pipeLosses_list)
        nodeLosses_list = np.sum(pipeLosses_list,axis=0) # sum by row (time), length of ts_index
        allNodeLosses.append(nodeLosses_list)
    return allNodeLosses

def nodeLossResults(n, l, ts_index):
    energyNodes = [node for node in list(n) if n[node].node_type==0]
    nodeloss_list = []
    nodeUnitloss_list = []
    for node in energyNodes:
        # list for pipes that connect
        pipes_connect = [i.index for i in n[node].links \
                         if l[i.index].link_type==1]
        # list for pipes that connect to a "specialty node"
        pipes_special = [pipe for pipe in pipes_connect \
                         if (l[pipe].start.index not in energyNodes) \
                         or (l[pipe].end.index not in energyNodes)]
        # sum(NOT special loss)/2 + sum(SPECIAL loss)
        special_loss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) \
                        for link in pipes_special]
        special_unitloss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent))/ \
                            l[link].results[leng][0] \
                            for link in pipes_special]
        notSpecial_loss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) \
                           for link in pipes_connect \
                           if link not in pipes_special]
        notSpecial_unitloss = [sum(getLinkValuesMid(link,hl,ts_index,l,percent))/ \
                               l[link].results[leng][0] \
                               for link in pipes_connect \
                               if link not in pipes_special]
        nodeloss = sum(notSpecial_loss)/2 + sum(special_loss)
        nodeUnitloss = sum(notSpecial_unitloss)/2 + sum(special_unitloss)
        nodeloss_list.append(nodeloss)
        nodeUnitloss_list.append(nodeUnitloss)
    return nodeloss_list, nodeUnitloss_list

def ageResults(junctions,n,l,ts_index):
    ageDist_list = []  # age distribution across times
    ageDistMax_list = []   # max age for each node
    age24hr_list = []   # age @ hour 24 for each node
    for node in junctions:
        ageDist_list.append(getNodeValues(node,q,ts_index,n))
        age24hr_list.append(n[node].results[q][-1])
        maxAge = max(getNodeValues(node,q,ts_index,n))
        ageDistMax_list.append(maxAge)
    # average water age @ hour 24
    ageAvg = sum(age24hr_list)/float(len(age24hr_list))
    # median water age @ hour 24
    ageMed = np.median(np.array(age24hr_list))
    return age24hr_list, ageAvg, ageMed

def leakResults(a,B,junctions,n,l,ts_index):
    # leakage parameters a, B
    # nodal leakage - computed from leakage along pipe model
    tot_bgLeaks = []
    # for each node
    for nodei in junctions:
        tot_qk_leak_list = []
        # get all pipes connected to node
        connections = sorted([c.index for c in n[nodei].links])
        # for each time step
        for time_idx in ts_index:
            # get node's pressure
            nodei_press = round(n[nodei].results[p][time_idx],2)
            qk_leak_list = []
            # for each connecting pipe
            for link in list(connections):
                # find the other node (node j)
                if l[link].start.index == nodei:
                    nodej = l[link].end.index
                else:
                    nodej = l[link].start.index
                # get other node's pressure
                nodej_press = round(n[nodej].results[p][time_idx],2)
                # pressure in pipe = mean of pressure @ nodes
                link_press = (nodei_press + nodej_press)/2
                # calculate pipe's leak
                if link_press > 0:
                    # nodal leakage flow = 0.5*sum(B*length*linkPress**a)
                    # 1ft = 0.3048m, 1.42197psi = 1m of head, \
                    # 15.8503gpm = 1L/s
                    qk_leak = 0.5*B*(l[link].results[leng][0]*0.3048)* \
                    ((link_press/1.42197)**a)*15.8503
                else:
                    qk_leak = 0
                qk_leak_list.append(qk_leak)
            # leak in node i = sum of connecting pipe leaks
            tot_qk_leak = sum(qk_leak_list)
            # add to list of leaks of this node @ all times
            tot_qk_leak_list.append(tot_qk_leak)
#            cut1, cut2, tot_qk_leak_list = jp.midPercentile(tot_qk_leak_list,percent,"")
        # calculate leakage of this node for whole time pd
        bgLeak = sum(tot_qk_leak_list)
        # add to list of leaks across entire network @ all times
        tot_bgLeaks.append(round(bgLeak,2))
    return tot_bgLeaks, sum(tot_bgLeaks)

def leakResults2(a,B,junctions,n,l,ts_index):
    leakNode_list = []
    for nodei in junctions:
        # connecting pipes
        connections = sorted([c.index for c in n[nodei].links])
        leakLink_list = []
        for link in list(connections):
            # get length
            lengthLink = l[link].results[leng][0] #ft
            # find the other node (node j)
            if l[link].start.index == nodei:
                nodej = l[link].end.index
            else:
                nodej = l[link].start.index
            # get all pressures of node i and node j
            pressi = np.array(getNodeValues(nodei,p,ts_index,n))
            pressj = np.array(getNodeValues(nodej,p,ts_index,n))
            # get all pressures of this link by taking average
            pressLink = 0.5*(pressi + pressj) #psi
            # take mid X% of the values
            cut1, cut2, pressLinkMid = jp.midPercentile(pressLink,percent,"")
            pressLinkMid = np.array(pressLinkMid)
            # calculate total leak @ this link = B*length*linkPress**a
            # 1ft = 0.3048m, 1.42197psi = 1m of head, 15.8503gpm = 1L/s
            leakLink = B*(lengthLink*0.3048)*((pressLinkMid/1.42197)**a)*15.8503
            # sum vector and add to list for all connecting links
            leakLink_list.append(sum(leakLink))
        # calculate total leak @ this node = 0.5*sum(all leakLinks)
        leakNode = 0.5*sum(leakLink_list)
        # add to list for all nodes
        leakNode_list.append(leakNode)
    leakNetwork = sum(leakNode_list)
    return leakNode_list, leakNetwork

def pressResults(n,ts_index):
    # junction list without I-Pump, O-Pump, I-RV, O-RV
    junctions = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]
    avgpress_list = []
    peakpress_list = []
    medpress_list = []
    allpressres_list = []
    for node in junctions:
        press_results = getNodeValues(node,p,ts_index,n)
        allpressres_list.append(press_results)
        # average
        avgpress = sum(press_results)/len(press_results)
        avgpress_list.append(avgpress)
        # peak
        peakpress = max(press_results)
        peakpress_list.append(peakpress)
        # median
        medpress = np.median(np.array(press_results))
        medpress_list.append(medpress)
    avgMedpress = np.mean(medpress_list)
    avgPeakPress = sum(peakpress_list)/len(peakpress_list)
    return avgpress_list, peakpress_list, medpress_list, \
            avgMedpress, avgPeakPress, allpressres_list

def flowResults(l,ts_index):
    avgflow_list = []
    peakflow_list = []
    for link in list(l):
        # for pipes
        if l[link].link_type==1:
            flow_results = getLinkValuesMid(link,flow,ts_index,l,percent)
            abs_flow_results = []
            for result in flow_results:
                abs_result = abs(result)
                abs_flow_results.append(abs_result)
            # average flows
            avgflow = sum(abs_flow_results)/len(abs_flow_results)
            avgflow_list.append(avgflow)
            # peak flows
            peakflow = max(abs_flow_results)
            peakflow_list.append(peakflow)
    return avgflow_list, peakflow_list

#def pumpEnergyResults(pumps_list,l,ts_index):
#    kwenergy_list = []
#    for pump in pumps_list:
#        # get energy in kwatts
#        kw_results = getLinkValues(link,e,ts_index,l)
#        flow_results = getLinkValues(link,flow,ts_index,l)
#        # get flow in MG
#        abs_flow_results = []
#        for result in flow_results:
#            abs_result = abs(result)
#            abs_flow_results.append(abs_result)
#        # gpm --> MGhr
#        MGhr_flow = sum(abs_flow_results)*60/(10**6)
#        kwenergy_list.append(sum(kw_results)/MGhr_flow)   #kw-hr/MG
#    return kwenergy_list
#
#def costFlowResults(weightParam,pipes_list,l,ts_index):
#    # returns the total cost of flow = sum over all edges of
#    # the product of the edge's flow and weight
#    weight = weightParam # typically head loss, for our purposes
#    eCostsList = []
#    for pipe in pipes_list:
#        # for 1 pipe @ all times
#        eFlow = np.array(np.fabs(getLinkValues(pipe,flow,ts_index,l)))
#        eHL = np.array(getLinkValues(pipe,weight,ts_index,l))
#        eCost = np.multiply(eFlow,eHL)
#        # add sum to list for all pipes
#        eCostsList.append(sum(eCost))
#    costOfFlow = sum(eCostsList)
#    return costOfFlow

#---START NETWORKS..-----------------------------------------------------
# For plots: lists of lists
pct_nloss_list = []
pct_nLeak_list = []
pct_age24hr_list = []
wloss_pctDiff = []
pflow_pctDiff = []
age_pctDiff = []
eloss_pctDiff = []

pctDiffwLoss = []
pctDiffwAge = []
pctDiffeLoss = []
pctDiffpFlow = []

allUnitLoss=[]
allLoss=[]
allnLeak=[]
allAge=[]
appList = []
apfList=[]
aafList = []
pumpEnergy = []
energyIntens = []
HLCostList = []
allUnitLossF=[]
allLossF=[]
allnLeakF=[]
allAgeF=[]
appListF=[]
apfListF=[]
aafListF = []
pumpEnergyF = []
energyIntensF = []
HLCostListF = []

nodeNoList=[]
pipeNoList=[]
pumpNoList=[]
lengthList=[]
totbdList=[]
lenOverDemList=[]
lenOverSourceList=[]

for net in networks:

    print "Network: ", net
    # File for network (.inp)
    file = net + '.inp'
    dir = str(hr) + '_hr_pattern'
    path = os.path.join(os.getcwd(), dir, file)

    es = EPANetSimulation(path) # EPANetSimulation opens & closes file

    # Define NOMINAL demand pattern
    p1_index = 1
    p1 = es.network.patterns[p1_index]
    p1_list = list(p1.values())[0:24]
    pat1 = p1_list
    avg_pat = round(sum(p1_list)/len(p1_list),4)

    # Define FLATTENED demand pattern
    p2_index = 2
    p2 = es.network.patterns[p2_index]
    p2_list = []
    [p2_list.append(avg_pat) for i in range(1,nt+1)]

    # Define REDUCED demand pattern
    p3_index = 3
    p3 = es.network.patterns[p3_index]
    p3_list = list(p3.values())[0:24]

#---NOMINAL PATTERN-----------------------------------------------------------

    # Set pattern
    ret = es.ENsetpattern(p1_index, p1_list)
    if ret != 0:
        print ret

    # Permanently change values = write to new file
    f1 = os.path.join(tempfile.gettempdir(), "temp.inp")
    ret = es.ENsaveinpfile(f1)
    if ret != 0:
        print ret

    # Open, run sim, close file with EPANetSim
    es1 = EPANetSimulation(f1)
    n = es1.network.nodes
    l = es1.network.links

    # Run new simulations
    error = es1.run()
    errorq = es1.runq()
    # get errors?
    print "Errors: ", error, errorq

    es1.ENsaveH()
    es1.ENreport()

    # Get correct time steps
    tsteps = es1.network.tsteps
    ts_index = getTimeSteps(tsteps)

    # NETWORK SIZE-------------------------------------------------------------
    pipes_list = np.array([pipe for pipe in list(l) if l[pipe].link_type==1])
    node_list = np.array(list(n))
    pumps_list = np.array([link for link in list(l) if l[link].link_type==2])
    res_list = np.array([node for node in list(n) if n[node].node_type==1])

    # nodes
    nodeNo = len(node_list)
    nodeNoList.append(nodeNo)

    # pipes
    pipeNo = len(pipes_list)
    pipeNoList.append(pipeNo)

    # pumps
    pumpNo = len(pumps_list)
    pumpNoList.append(pumpNo)

    # length of pipes (Kft)
    length = sum([l[link].results[leng][0] for link in list(l)]) / 1000
    lengthList.append(round(length,2))

    # total base demand (MGD)
    basedem = [n[node].results[b][0] for node in list(n) \
               if n[node].node_type==0]
    totbasedem = sum(basedem) * 60 * 24 / (10**6)
    totbdList.append(round(totbasedem,2))

#    # L/D (Kft/MGD)
#    lenOverDemList.append(round(length/totbasedem,2))
#
#    # L/# sources
#    # sources = reservoirs, pumping stations (pumps not connected to reservoir)
#    resNo = len(res_list)
#    pumpNodes = [node for node in list(n) if 'P' in n[node].id]
#    resPumpNo = 0
#    for node in pumpNodes:
#        nodeConnections = [c.index for c in n[node].links]
#        for c in nodeConnections:
#            if 'R' in l[c].start.id:
#                resPumpNo += 1
#    sourceNo = resNo + (pumpNo - resPumpNo)
#    lenOverSourceList.append(length/sourceNo)

    # RESULTS------------------------------------------------------------------

    # check for nodes recording negative or low pressure (PSI)
    pNeg = 0
    pLow = 40   # considered low pressure
    pHi = 100   # max psi
    neg_press = sorted([y.id for x,y in n.items() \
                        if round(min(y.results[p]),1)<pNeg])
    low_press = sorted([y.id for x,y in n.items() \
                        if round(min(y.results[p]),1)<pLow \
                        and y.id not in neg_press])
    hi_press = sorted([y.id for x,y in n.items() \
                       if round(min(y.results[p]),1)>pHi])

    # junction list without I-Pump, O-Pump, I-RV, O-RV
    junctions = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]

    # HEAD (ENERGY) LOSSES @ all times (ft)
    # for ALL PIPES
    tot_losses = [sum(getLinkValuesMid(link,hl,ts_index,l,percent)) for link in list(l) \
                  if l[link].link_type==1]
#    unit_losses = [sum(getLinkValuesMid(link,hl,ts_index,l,percent))/(l[link].results[leng][0]) for link in list(l) \
#                  if l[link].link_type==1] #ft/ft
#    allUnitLoss.append(round(sum(unit_losses)*30/1000,2)) #ft/Kft
    # for ALL JUNCTIONS
    # energy loss at node = sum(loss of all connecting pipes)/2
    # but if the connecting pipe has tank/res, then junction takes ALL loss
    # [energy losses, unit energy losses]
    nodeloss_list, nodeUnitloss_list = nodeLossResults(n,l,ts_index)
    nLossResult = round(sum(nodeloss_list)*30,2) #ft
    allLoss.append(nLossResult) #ft

    # WATER AGE distribution for ea nodes @ all times (hr)
    # [age at 24 hours, average age at 24 hours, median age at 24 hours]
    age24hr_list, ageAvg, ageMed = ageResults(junctions,n,l,ts_index)
    ageResult = round(ageMed,2) #hr
    allAge.append(ageResult) #hr

    # BACKGROUND LEAKAGE (gpm)
    # leakage parameters
    a = 1.5 # for background losses through area than changes linearly w/ press (May 1994)
    B = 10**(-7)    # a reasonable initial guess from _______
    # nodal leakage - computed from leakage along pipe model
    tot_bgLeaks, leakSum = leakResults2(a,B,junctions,n,l,ts_index)
#    allnLeak.append(round(leakSum*30/(10**6),2)) #MGD
    wLossResult = round(leakSum*30,2) #gpd
    allnLeak.append(wLossResult) #gpd

    # FLOWS (gpm)
    avgflow_list, peakflow_list = flowResults(l,ts_index)
    pkFlowResult = round(np.median(peakflow_list),2) #gpm
    apfList.append(pkFlowResult) #gpm
    aafList.append(round(sum(avgflow_list)/len(avgflow_list),2)) #gpm?

    # PRESSURES (psi)
    avgpress_list, peakpress_list, medpress_list, \
    avgMedpress, avgPeakPress, allPressResults = pressResults(n,ts_index)
    pkPressResult = avgPeakPress #psi
    appList.append(avgPeakPress) #psi

#    # PUMP ENERGY (kw-hr/MG)
#    pumpEnergy_list = pumpEnergyResults(pumps_list,l,ts_index)
#    ePumpResult = round(sum(pumpEnergy_list),2)
#    pumpEnergy.append(ePumpResult)
#
#    # ENERGY INTENSITY (ft/MGD)
#    eIntensResult = round(sum(nodeloss_list)*30/totbasedem,2)
#    energyIntens.append(eIntensResult)
#
#    # COST OF FLOW - HEAD LOSS (ft*gpm)
#    totCost = costFlowResults(hl,pipes_list,l,ts_index)
#    totCostResult = round(totCost,2)
#    HLCostList.append(totCostResult)

##---PRINT STATEMENTS FOR NOMINAL-----------------------------------------
##
#    print "For the NOMINAL DEMAND PATTERN:"
##    print ""
##    print "Pressure < 0: ", neg_press
##    print "Pressure < 40: ", len(low_press)
##    print "Pressure > 100: ", len(hi_press)
##    print avgMedpress, "psi"
##
##    # leak number every 30 min (plus a few extra), therefore multiply by 30 min
#    print "Total background leakage is ", round(wLossResult/10**6,3), " MGD"
#
#    print "The median water age for all nodes @ 24 hrs is ", ageResult, " hr"
##    # loss number every 30 min (plus a few extra), therefore multiply by 30 min
##    print "The total head loss in pipes is " , round(sum(tot_losses)*30,2), " ft"
#    print "The total head loss in junctions is " , round(nLossResult/1000,3), " k-ft"
##    print "The total unit head loss in junctions is ", round(sum(nodeUnitloss_list)*30,2), " ft/ft"
##
#    print "Median peak flow is", pkFlowResult, "gpm"
##
##    # Pattern (check)
##    pattern = [round(es1.ENgetpatternvalue(p1_index,i)[1],2) for i in range(1,nt+1)]
##    print pattern
##    print len(pattern)
##
##---RESULTS 2.0-------------------------------------------------------------
#    scenario = 'Nom'
#    #
#    for metric in metrics:
#        if metric == 'flow':
#            features = 'pipes'
#            prop = flow
#        elif metric == 'headloss':
#            features = 'nodes'
#            prop = hl
#        elif metric == 'pressure':
#            features = 'junctions'
#            prop = p
#        else:
#            features = 'junctions'
#            prop = q
#        #
#        percent = 95
#        #
#    #   scenario = new #new or 'Nom'
#        if scenario == 'Nom':
#            ts_index = ts_index
#    #    else:
#    #        ts_index = ts_indexF
#        if metric == 'headloss':
#            propArray = nodeLossAll(n,l,ts_index)
#            dfStrip = pd.DataFrame(propArray)
#            dfStrip.columns = range(0,len(ts_index))
#        else:
#            dfStrip, propArray = jp.df4MatrixPlot_all(features,prop,ts_index,n,l,metric,scenario,net)
#        # for water age, only want last timestep
#        if metric == 'age':
#            dfStrip = dfStrip.iloc[:,-1]
#            dfStrip = pd.concat([dfStrip,dfStrip],axis=1)
#            propArray = propArray[-1]
#        elif metric == 'flow':
#            dfStrip = abs(dfStrip)
#            propArray = abs(np.array(propArray))
#        cut1, cut2, dataNew = jp.midPercentile(propArray,percent,metric)
#        plt.close()
#
#        #---- PLOT MATRIX -----
#        ylabel = features
#        xlabel = 'Time Step (hr)'
#        # all data
#        fig = plt.figure(figsize=[10,10])
#        sns.heatmap(dfStrip, center=0, cmap='RdBu_r', xticklabels=np.arange(0,24.5,0.5))
#        plt.title(net+' '+metric+' '+scenario+'-All Values Matrix Plot')
#        plt.ylabel(ylabel)
#        plt.xlabel(xlabel)
##        fig.savefig(net+metric + scenario + 'All_MatrixPlot.pdf',format='pdf', dpi=100)
#        plt.close(fig)
#        # 95th
#        fig = plt.figure(figsize=[10,10])
#        sns.heatmap(dfStrip, vmin=cut1, vmax=cut2, center=0, cmap='RdBu_r', xticklabels=np.arange(0,24.5,0.5))
#        plt.title(net+' '+metric+' '+scenario+'-'+str(percent)+'% Values Matrix Plot')
#        plt.ylabel(ylabel)
#        plt.xlabel(xlabel)
#        fig.savefig(net+metric + scenario + str(percent) + '_MatrixPlot.pdf',format='pdf', dpi=100)
#        plt.close(fig)


#---NEW PATTERN--------------------------------------------------------

    if new == 'Flat':
        # Set FLAT pattern (changing pattern 1 to values of flattened)
        ret = es1.ENsetpattern(p1_index, p2_list)
        if ret != 0:
            print ret
        avgpat = 1
        scen = '2'
    else:
        # Set REDUCED pattern (changing pattern 1 to values of reduced)
        ret = es1.ENsetpattern(p1_index, p3_list)
        if ret != 0:
            print ret
        avgpat = 0.5
        scen = '1'

    # Permanently change values = write to new file
    fNew = os.path.join(tempfile.gettempdir(), "tempf.inp")
    ret = es1.ENsaveinpfile(fNew)
    if ret != 0:
        print ret

    # Open, run sim, close file with EPANetSim
    esf = EPANetSimulation(fNew)
    nF = esf.network.nodes
    lF = esf.network.links

    # Run new sims
    error = esf.run()
    errorq = esf.runq()
    # get errors?
    print "Errors: ", error, errorq

    # Get correct time steps
    tstepsF = esf.network.tsteps
    ts_indexF = getTimeSteps(tstepsF)

    # RESULTS------------------------------------------------------------------

    # check for nodes recording negative & low pressure (psi)
    neg_pressF = sorted([y.id for x,y in nF.items() \
                        if round(min(y.results[p]),1)<pNeg])
    low_pressF = sorted([y.id for x,y in nF.items() \
                        if round(min(y.results[p]),1)<pLow \
                        and y.id not in neg_press])
    hi_pressF = sorted([y.id for x,y in nF.items() \
                       if round(min(y.results[p]),1)>pHi])

    # get HEAD (ENERGY) LOSSES
    # for ALL pipes @ all times (ft)
    tot_lossesF = [sum(getLinkValuesMid(link,hl,ts_indexF,lF,percent)) for link in list(lF) \
                   if lF[link].link_type==1]
#    unit_lossesF = [sum(getLinkValues(link,hl,ts_indexF,lF))/(l[link].results[leng][0]) for link in list(lF) \
#                  if lF[link].link_type==1] #ft/ft
#    allUnitLossF.append(round(sum(unit_lossesF)*30/1000,2)) #ft/Kft
    # for all NODES
    nodelossF_list, nodeUnitlossF_list = nodeLossResults(nF,lF,ts_indexF)
    nLossResultF = round(sum(nodelossF_list)*30,2) #ft
    allLossF.append(nLossResultF) #ft

    # get WATER AGE distribution for ea nodes @ all times
    age24hrF_list, ageAvgF, ageMedF = ageResults(junctions,nF,lF,ts_indexF)
    ageResultF = round(ageMedF,2) #hr
    allAgeF.append(ageResultF) #hr

    # calculate BACKGROUND LEAKAGE
    # leakage parameters same as nominal
    # nodal leakage - computed from leakage along pipe model
    tot_bgLeaksF, leakSumF = leakResults2(a,B,junctions,nF,lF,ts_indexF)
#    allnLeakF.append(round(leakSumF*30/(10**6),2)) #MGD
    wLossResultF = round(leakSumF*30,2) #gpd
    allnLeakF.append(wLossResultF) #gpd

    # FLOWS (gpm)
    avgflowF_list, peakflowF_list = flowResults(lF,ts_indexF)
    pkFlowResultF = round(np.median(peakflowF_list),2)
    apfListF.append(pkFlowResultF) #gpm
    aafListF.append(round(sum(avgflowF_list)/len(avgflowF_list),2)) #gpm?

    # PRESSURES (psi)
    avgpressF_list, peakpressF_list, medpressF_list, \
    avgMedpressF, avgPeakPressF, allPressResultsF = pressResults(nF,ts_indexF)
    pkPressResultF = avgPeakPressF #psi
    appListF.append(pkPressResultF)

#    # PUMP ENERGY (kw-hr/MG)
#    pumpEnergyF_list = pumpEnergyResults(pumps_list,lF,ts_indexF)
#    pumpEnergyF.append(round(sum(pumpEnergyF_list),2))
#
#    # ENERGY INTENSITY (ft/MGD)
#    energyIntensF.append(round(sum(nodelossF_list)*30/(totbasedem*avgpat),2))
#
#    # COST OF FLOW - HEAD LOSS (ft*gpm)
#    totCostF = costFlowResults(hl,pipes_list,lF,ts_indexF)
#    HLCostListF.append(round(totCostF,2))


##---PRINT STATEMENTS FOR NEW----------------------------------------------
##
#    print ""
#    if new == 'Flat':
#        print "For the FLATTENED DEMAND PATTERN:"
#    else:
#        print "For the REDUCED DEMAND PATTERN:"
##    print ""
##
##    print "Pressure < 0: ", neg_pressF
##    print "Pressure < 40: ", len(low_pressF)
##    print "Pressure > 100: ", len(hi_pressF)
##    print avgMedpressF, "psi"
##    print ""
##
#    print "Total background leakage is ", round(wLossResultF/10**6,3), " MGD"
#
#    print "Median water age for all nodes @ 24 hrs is ", ageResultF, " hr"
#
##    print "The total head loss in pipes is " , round(sum(tot_lossesF)*30,2), " ft"
#    print "The total head loss in junctions is " , round(nLossResultF/1000,3), " k-ft"
##    print "The total unit head loss in junctions is ", round(sum(nodeUnitlossF_list)*30,2), " ft/ft"
##
#    print "Median peak flow is", pkFlowResultF, "gpm"
#    print ""
##
##    pattern = [round(esf.ENgetpatternvalue(p1_index,i)[1],2) for i in range(1,nt+1)]
##    print pattern
##    print len(pattern)

##---PATTERN PLOT--------------------------------------------------------------
#    time_L = esf.network.time
#    times = [t/3600 for t in time_L if (t % 3600 == 0)]
##    times = np.arange(0,49,2)
##    times = times[1::]
##    timesTicks = np.arange(0,49,2)
#    times = range(1,25)
#    timesTicks = ['4:00', '8:00', '12:00', '16:00', '20:00', '24:00']
#    yTicks = np.arange(0,2.5,0.5)
#    fsize=14 # 14pt for 0.75 latex scale to be like 12pt
#    reduced = 0.50  # reduced demand percentage (Gurung et al. 2015)
#    p3_list = np.round(np.array(p1_list) * reduced,2)
#
#    fig = plt.figure(figsize=(8,4))
#    ax = fig.add_subplot(111)
#    plt.plot(times, p1_list, '-', color='grey', markerfacecolor='none', markersize=8, linewidth=3)
#    plt.plot(times, p3_list, '--', color='C0', markerfacecolor='none', markersize=8, linewidth=3)
#    plt.plot(times, p2_list, '-', color='yellowgreen', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p1_list, 'C7-', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p3_list, 'k--', markerfacecolor='none', markersize=8, linewidth=3)
##    plt.plot(times, p2_list, 'k-', markerfacecolor='none', markersize=8, linewidth=3)
#
#    plt.legend(['S0',
#                'S1',
#                'S2'],frameon=False,fontsize=fsize,loc=9,bbox_to_anchor=(0.5,-0.15),ncol=3)
#    plt.xlabel('Time',fontsize=fsize)
#    plt.ylabel('Demand Multiplier',fontsize=fsize)
#    plt.xlim(1,max(times))
#    plt.xticks(range(4,25,4), timesTicks,fontsize=fsize)
#    plt.ylim(0,max(p1_list)+0.25)
#    plt.yticks(yTicks,yTicks,fontsize=fsize)
#    ax.yaxis.set_major_formatter(FormatStrFormatter('%0.1f'))
#    fig.tight_layout()
#    fig.subplots_adjust(bottom=0.21)
##    plt.show()
#    fig.savefig(dir + '/pattern_Thesis.pdf', format='pdf', dpi=600)
##    plt.close(fig)

###---%DIFF --------------------------------------------------------------

    # Delta Age @ 24 hr by Node (New - Base)
    delta_age24hr, pct_age24hr = jp.pctDiff(age24hr_list, age24hrF_list)
    pct_age24hr_list.append(pct_age24hr)
    age_pctDiff.append(np.median(pct_age24hr))

    # Background Leakage by Node (New - Base)
    delta_nLeak, pct_nLeak = jp.pctDiff(tot_bgLeaks, tot_bgLeaksF)
    pct_nLeak_list.append(pct_nLeak)
    wloss_pctDiff.append(np.median(pct_nLeak))

    # Energy Loss by Pipe (New - Base)
    delta_loss, pct_loss = jp.pctDiff(tot_losses, tot_lossesF)
    # Energy Loss by Node (New - Base)
    delta_nloss, pct_nloss = jp.pctDiff(nodeloss_list, nodelossF_list)
    pct_nloss_list.append(pct_nloss)
    eloss_pctDiff.append(np.median(pct_nloss))
##    # Unit Energy Loss by Node (New - Base)
##    delta_nUnitloss, pct_nUnitloss = jp.pctDiff(nodeUnitloss_list, nodeUnitlossF_list)
#s
    # Flow, average
    delta_avgflow, pct_avgflow = jp.pctDiff(avgflow_list, avgflowF_list)
    # Flow, peak
    delta_peakflow, pct_peakflow = jp.pctDiff(peakflow_list, peakflowF_list)
    pflow_pctDiff.append(np.median(pct_peakflow))

    dfOrderPipe = pd.DataFrame(delta_peakflow, columns=['PeakFlow'])
    dfOrderPipe['PipeIndex'] = pipes_list
    dfOrderPipe = dfOrderPipe.sort_values(by='PeakFlow')
    dfOrderPipe = dfOrderPipe.reset_index(drop=True)

    # Pressure, average
    delta_avgpress, pct_avgpress = jp.pctDiff(avgpress_list, avgpressF_list)
    # Pressure, peak
    delta_peakpress, pct_peakpress = jp.pctDiff(peakpress_list, peakpressF_list)
    # Pressure, median
    delta_allPressRes, pct_allPressRes = jp.pctDiff(allPressResults, allPressResultsF)

#    print "median delta pressure:", round(np.median(delta_allPressRes.flatten()),2), "psi"
#    print "delta water loss:", round((wLossResultF - wLossResult)/10**6,3), "MGD"
#    print "median delta age @ 24hr:", round(np.median(delta_age24hr),2), "hr"
#    print "delta energy loss:", round((nLossResultF - nLossResult)/1000,3), "k-ft"
#    print "median delta peak flow:", round(np.median(delta_peakflow),2), "gpm"
#    print ""
#    print "pct diff water loss:", round(100*(wLossResultF - wLossResult)/wLossResult,2), "%"
#    print "pct diff water age 1:", round(np.median(pct_age24hr),2)
#    print "pct diff water age 2:", round(100*np.median(delta_age24hr)/np.median(age24hr_list),2)
#    print "pct diff energy loss:", round(100*(nLossResultF - nLossResult)/nLossResult,2)
#    print "pct diff peak flow 1:", round(np.median(pct_peakflow),2)
#    print "pct diff peak flow 2:", round(100*np.median(delta_peakflow)/np.median(peakflow_list),2)
#    print ""
#    pctDiffwLoss.append(100*(wLossResultF - wLossResult)/wLossResult)
#    pctDiffwAge.append(100*(np.median(delta_age24hr)/np.median(age24hr_list)))
#    pctDiffeLoss.append(100*(nLossResultF - nLossResult)/nLossResult)
#    pctDiffpFlow.append(100*(np.median(delta_peakflow)/np.median(peakflow_list)))

    pctDiffwLoss.append(100*(wLossResultF - wLossResult)/wLossResult)
    pctDiffwAge.append(np.median(pct_age24hr))
    pctDiffeLoss.append(100*(nLossResultF - nLossResult)/nLossResult)
    pctDiffpFlow.append(np.median(pct_peakflow))


    dfOrderJunc = pd.DataFrame(delta_avgpress, columns=['AvgPress'])
    dfOrderJunc['JuncIndex'] = junctions
    dfOrderJunc = dfOrderJunc.sort_values(by='AvgPress')
    dfOrderJunc = dfOrderJunc.reset_index(drop=True)

##---WRITE TO EXCEL FOR PLOT----------------------------------------------------
#df_filename = 'All-Net_Info_95PMay.xlsx'
#dfNet = pd.DataFrame({'Network': networks, 'No. Nodes': nodeNoList, \
#                'No. Pipes': pipeNoList, \
##                'No. Pumps': pumpNoList, \
#                'Tot Pipe Length (Kft)': lengthList, \
#                'Tot Base Demand (MGD)': totbdList, \
##                'L/D (Kft/MGD)': lenOverDemList, \
##                'L/No.Sources (Kft)': lenOverSourceList \
#        })
#dfBase = pd.DataFrame({'Leakage (MGD)-Base': np.array(allnLeak)/10**6, \
#                    'Age (hr)-Base': allAge, \
#                    'Peak Flow (gpm)-Base': apfList, \
#                    'Energy Loss (k-ft)-Base': np.array(allLoss)/1000, \
##                    'Unit Energy Loss (ft/Kft)-Base': allUnitLoss,\
##                    'Energy Intensity (ft/MGD)-Base': energyIntens,\
##                    'Pump Energy (kwhr/MG)-Base': pumpEnergy, \
##                    'Cost of Flow-Base': HLCostList \
#        })
#if new == 'Reduc':
#    dfReduc = pd.DataFrame({'Leakage (MGD)-' +new: np.array(allnLeakF)/10**6, \
#                        'Age (hr)-' +new: allAgeF, \
#                        'Peak Flow (gpm)-' +new: apfListF, \
#                        'Energy Loss (k-ft)-' +new: np.array(allLossF)/1000, \
##                        'Unit Energy Loss (ft/Kft)-' +new: allUnitLossF, \
##                        'Energy Intensity (ft/MGD)-' +new: energyIntensF, \
##                        'Pump Energy (kwhr/MG)-' +new: pumpEnergyF, \
##                        'Cost of Flow-' +new: HLCostListF \
#        })
#    dfPct_Reduc = pd.DataFrame({'Leakage (%)-' +new: pctDiffwLoss, \
#                        'Age (%)-' +new: pctDiffwAge, \
#                        'Peak Flow (%)-' +new: pctDiffpFlow, \
#                        'Energy Loss (%)-' +new: pctDiffeLoss, \
#                        })
#    frames = [dfNet,dfBase,dfReduc,dfPct_Reduc]
#else:
#    dfFlat = pd.DataFrame({'Leakage (MGD)-' +new: np.array(allnLeakF)/10**6, \
#                        'Age (hr)-' +new: allAgeF, \
#                        'Peak Flow (gpm)-' +new: apfListF, \
#                        'Energy Loss (k-ft)-' +new: np.array(allLossF)/1000, \
##                        'Unit Energy Loss (ft/Kft)-' +new: allUnitLossF, \
##                        'Energy Intensity (ft/MGD)-' +new: energyIntensF, \
##                        'Pump Energy (kwhr/MG)-' +new: pumpEnergyF, \
##                        'Cost of Flow-' +new: HLCostListF \
#    })
#    dfPct_Flat = pd.DataFrame({'Leakage (%)-' +new: pctDiffwLoss, \
#                        'Age (%)-' +new: pctDiffwAge, \
#                        'Peak Flow (%)-' +new: pctDiffpFlow, \
#                        'Energy Loss (%)-' +new: pctDiffeLoss, \
#                        })
#
#    dfRead = pd.read_excel(df_filename)
#    frames = [dfRead,dfFlat,dfPct_Flat]
#
#df = pd.concat(frames, axis=1)
#df.to_excel(df_filename, sheet_name='Sheet1',index=False)


#---- RESULTS ROUND 2 -----
#    scenario = new #new or 'Nom'



##---- MATRICES/DATAFRAMES OF DATA, NO AVERAGING -----
#    #
#    for metric in metrics:
#        if metric == 'flow':
#            features = 'Pipes'
#            prop = flow
#            unit = ' (gpm) '
#            label = 'Flow'
#            label2 = 'Peak Flow'
#            ascend = False
#            deltaX = delta_peakflow
#        elif metric == 'headloss':
#            features = 'Nodes'
#            prop = hl
#            unit = ' (ft) '
#            label = 'Energy Loss'
#            deltaX = delta_nloss
#        elif metric == 'pressure':
#            features = 'Junctions'
#            prop = p
#            unit = ' (psi) '
#            label = 'Pressure'
#            ascend = True
#            deltaX = delta_allPressRes
#        elif metric == 'waterloss':
#            features = 'Junctions'
#            unit = ' (gpd) '
#            label = 'Water Loss'
#            deltaX = delta_nLeak
#        else:
#            features = 'Junctions'
#            prop = q
#            unit = ' (hr) '
#            label = 'Water Age'
#            deltaX = delta_age24hr
#        #
#        percent = 95
#        #
#    #    if scenario == 'Nom':
#    #        ts_index = ts_index
#    #    else:
#    #        ts_index = ts_indexF
#        if metric == 'headloss':
#            propArray = nodeLossAll(n,l,ts_index)
#            propArrayF = nodeLossAll(nF,lF,ts_indexF)
#            dfStrip = pd.DataFrame(propArray)
#            dfStrip.columns = range(0,len(ts_index))
#            dfStripF = pd.DataFrame(propArrayF)
#            dfStripF.columns = range(0,len(ts_indexF))
#        elif metric == 'waterloss':
#            propArray = np.array(tot_bgLeaks)
#            propArrayF = np.array(tot_bgLeaksF)
#            dfStrip = pd.DataFrame(propArray)
#            dfStripF = pd.DataFrame(propArrayF)
#        else:
#            dfStrip, propArray = jp.df4MatrixPlot_all(features,prop,ts_index,n,l,metric,'Nom',net)
#            dfStripF, propArrayF = jp.df4MatrixPlot_all(features,prop,ts_indexF,nF,lF,metric,scenario,net)
#        # for water age, only want last timestep
#        if metric == 'age':
#            dfStrip = dfStrip.iloc[:,-1]
#            dfStrip = pd.concat([dfStrip,dfStrip],axis=1)
#            dfStripF = dfStripF.iloc[:,-1]
#            dfStripF = pd.concat([dfStripF,dfStripF],axis=1)
#            propArrayF = propArrayF[-1]
#        elif metric == 'flow':
#            dfStrip = abs(dfStrip)
#            propArray = abs(np.array(propArray))
#            dfStripF = abs(dfStripF)
#            propArrayF = abs(np.array(propArrayF))
#        cut1, cut2, dataNew = jp.midPercentile(propArrayF,percent,metric)
#        plt.close('all')
#
##        #---- REORDER DF BY DELTA
##        dfStrip["Med"] = dfStrip.median(axis=1)
##        dfStrip = dfStrip.sort_values(by="Med")
##        df = dfStrip.reset_index() # don't drop prev index bc it's feature index-1
##
##        dfStripF["Med"] = dfStripF.median(axis=1)
##        dfStripF = dfStripF.sort_values(by="Med")
##        dfF = dfStripF.reset_index() # don't drop prev index bc it's feature index-1
##
##        dfStrip = df[df.columns.difference(['index','Med'])]
##        dfStripF = dfF[dfF.columns.difference(['index','Med'])]
##
##        #---- PLOT MATRIX -----
#        ylabel = features
#        xlabel = 'Time (hr)'
##        # all data
###        fig = plt.figure(figsize=[10,10])
###        sns.heatmap(dfStripF, center=0, cmap='RdBu_r', xticklabels=np.arange(0,24.5,0.5))
###        plt.title(net+' '+metric+' '+scenario+'-All Values Matrix Plot')
###        plt.ylabel(ylabel)
###        plt.xlabel(xlabel)
###        fig.savefig(net+metric + scenario + 'All_MatrixPlot.pdf',format='pdf', dpi=100)
###        plt.close(fig)
##        # 95th
###        fig = plt.figure(figsize=[10,10])
###        sns.heatmap(dfStripF, vmin=cut1, vmax=cut2, center=0, cmap='RdBu_r', xticklabels=np.arange(0,24.5,0.5))
###        plt.title(net+' '+metric+' '+scenario+'-'+str(percent)+'% Values Matrix Plot')
###        plt.ylabel(ylabel)
###        plt.xlabel(xlabel)
###        fig.savefig(net+metric + scenario + str(percent) + '_MatrixPlot.pdf',format='pdf', dpi=100)
###        plt.close(fig)
##        ## no outliers - doesn't make sense for flows
##        #cap1, cap2 = jp.noOutliers(propArrayF,metric)
##        #fig = plt.figure(figsize=[20,20])
##        #sns.heatmap(dfStripF, vmin=cap1, vmax=cap2)
##
#        #---- PLOT MATRIX - DIFFERENCES (NEW - NOMINAL) -----
#        #         may have to do this:
#        dfStripF.columns = dfStrip.columns
#        dfDiff = dfStripF.subtract(dfStrip)
#        #---- REORDER DF BY DELTA
#        dfDiff["Med"] = dfDiff.median(axis=1)
#        dfDiff = dfDiff.sort_values(by="Med", ascending=ascend)
#        df = dfDiff.reset_index() # don't drop prev index bc it's feature index-1
#
#        dfDiff = df[df.columns.difference(['index','Med'])]
#
#
###------ # the heatmap matrix plots, 95%
##        fontsize = 22
##        sns.set(font_scale=1.8)
##        data = dfDiff
##        data.columns = np.arange(0,24.5,0.5)
##        cut1, cut2, dataNew = jp.midPercentile(dfDiff,percent,metric)
##        fig, ax = plt.subplots(figsize=[7,8])
##        sns.heatmap(data, ax=ax, vmin=cut1, vmax=cut2, center=0, cmap='RdBu_r',\
##                    yticklabels=False, xticklabels=12, \
##                    cbar_kws={'label':r'$\Delta$ '+label+unit, 'shrink':0.75})
###        plt.title(net+' '+metric+' '+scenario+'-Nom Matrix Plot')
##        ax.tick_params(axis='both', which='major', labelsize=fontsize)
##        plt.ylabel(ylabel, fontsize=fontsize)
##        plt.xlabel(xlabel, fontsize=fontsize)
##        plt.tight_layout()
##        fig.savefig(scen+'-HeatMatrix'+metric+'_Net'+net[-1]+'.pdf',format='pdf', dpi=100)
##        plt.close(fig)
#
#
####        dfDiff.to_csv(net + str(metric) + new + '-Nom.csv', index=False)
###
###        # statistical plots
###        fig = plt.figure(figsize=[9,9])
###        # scatterplot - nom v new
###        ax = fig.add_subplot(2,2,1)
###        x = dfStrip.values.flatten()
###        y = dfStripF.values.flatten()
###        ax.scatter(x,y,s=2)
###        ax.set_title(net+' '+metric+' '+scenario+' v. Nom Scatter Plot')
###        ax.set_ylabel(new+' '+metric)
###        ax.set_xlabel('Nom '+metric)
###        if metric == "flow":
###            ax = fig.add_subplot(2,2,4)
###            x = np.array(peakflow_list)
###            y = np.array(peakflowF_list)
###            yint = 0.0
###            slope = 1.0
###            ax.scatter(x,y,s=5)
###            ax.plot(x, slope*x+yint, 'r', lw=0.75)
###            ax.set_title(net+' peak '+metric+' '+scenario+' v. Nom Scatter Plot')
###            ax.set_ylabel(new+' peak '+metric)
###            ax.set_xlabel('Nom peak '+metric)
###
###        # qq plot - differences
###        ax = fig.add_subplot(2,2,2)
###        data = dfDiff.values.flatten()
###        data = np.array(data)
###        sm.qqplot(data, line='s', ax=ax)
###        ax.set_title(net+' '+metric+' '+scenario+'-Nom 95%QQ Plot')
###
###        #cdf - differences
###        data = dfDiff.values.flatten()
###        data = np.array(data)
###        data = np.sort(data)
###        nsize = len(data)
###        rank = np.arange(1,nsize+1)
###        cumulfreq = np.divide(rank,float(nsize+1))
###        ax = fig.add_subplot(2,2,3)
###        ax.plot(data,cumulfreq, 'o')
###        ax.set_title(net+' '+metric+' '+scenario+'-Nom 95%cdf')
###        ax.set_xlabel(metric)
###
###        fig.tight_layout()
####        fig.savefig(net+metric+scenario+'-Nom_statPlots.pdf',format='pdf', dpi=100)
###        plt.close(fig)
###
#
##------------------------ aggregated over time and features plots
#
#        if metric != 'age':
#            # Get flow 49 flow data points for each node/pipe
#            # Add each of these lists to a list = data
#            data = dfDiff
#
#            # multiple boxplots on one Axes
#            fig, ax = plt.subplots()
#            medianprops = dict(linestyle=None, linewidth=0)
#            boxprops = dict(color='C7')
#            whiskerprops = dict(color='C7')
#            test = ax.boxplot(data, showcaps=False, showfliers=False, \
#                              showmeans=False, medianprops=medianprops, \
#                              boxprops=boxprops, whiskerprops=whiskerprops)
#            medians = [round(test['medians'][i].get_ydata()[0],2) for i in range(0,len(dfDiff))]
#            #upWhiskers = [test['whiskers'][i].get_ydata()[0] for i in range(0,len(dfDiff))]
#            #lowWhiskers = [test['whiskers'][i].get_ydata()[1] for i in range(0,len(dfDiff))]
#            mediansPos = [v for v in medians if v > 0]
#            mediansNeg = [v for v in medians if v <= 0]
##            imed = 0
##            for v in medians:
##                # last time v > 0 is split between positiive and negative values
##                if v > 0:
##                    stop = imed
##                imed += 1
##            ax.plot(range(0,stop+1),mediansPos,'C3-')
##            ax.plot(range(stop+1,len(medians)),mediansNeg,'C0-')
##            #ax.plot(upWhiskers,'C0-.')
##            #ax.plot(upWhiskers,'C2-.')
##            ax.set_title(net+' '+metric+' '+scenario+'-Nom TimeAggregate')
##            ax.set_xlabel('Sorted by median delta')
##            ax.set_ylabel(metric)
##            ax.get_xaxis().set_ticklabels([])
##            ax.get_xaxis().set_ticks([])
###            fig.savefig(net[-1]+metric+scen+'-Nom_TimeAggPlot.pdf',format='pdf', dpi=100)
##            plt.close(fig)
##
#            # transpose dfDiff
#            dfDiff_T = dfDiff.transpose()
#            dfDiff_T = dfDiff_T.reset_index()
#            dfDiff_T = dfDiff_T.drop('index',axis=1)
#
##------     Feature values (american flag) distribution plots
#            fsize=20 # for trim + 0.48 scale
#            fig, ax = plt.subplots()
#            plt.axhline(0, color='C7', linewidth=0.35, alpha = 0.75)
#    #        array = dfDiff_T.values.flatten()
#    #        cut1, cut2, diff_TNew = jp.midPercentile(array,95,metric)
#            for i in range(0, dfDiff_T.shape[0]):
#                subset = dfDiff_T.iloc[i,:]
#                cut1, cut2, subsetNew = jp.midPercentile(subset,95,metric)
#                mid95 = [v for v in subset if (v>=cut1 and v<=cut2)]
#                tails = [v for v in subset if v not in mid95]
#    #            ax.plot(i*np.ones(len(subset)), subset, '-oC0',markersize=2)
#                ax.plot(i*np.ones(len(tails)), tails, 'C7',markersize=0, alpha=0.75)
##                ax.plot(i*np.ones(len(mid95)), mid95, 'k',markersize=0,linewidth=2)
#                ax.plot(i*np.ones(len(mid95)), mid95, 'C0',markersize=0,linewidth=3)
##            if metric != "age":
##                #t=40
##                trackSubset = dfDiff_T.iloc[40,:].tolist()
##                maxSubset = max(trackSubset)
##                minSubset = min(trackSubset)
##        #        medNode = 251
##                maxNode = trackSubset.index(maxSubset)
##                minNode = trackSubset.index(minSubset)
##        #        ax.plot(dfDiff_T[medNode], '-k')
##                ax.plot(dfDiff_T[maxNode], '-k')
##                ax.plot(dfDiff_T[minNode], '-k')
#
##            ax.set_title(net+' '+metric+' '+scenario+'-Nom Node Aggregate')
#            ax.set_xlabel('Time (hr)',fontsize=fsize)
#            ax.set_ylabel(r'$\Delta$ '+metric+unit, fontsize=fsize)
#            ax.set_xticks(range(0,49,12))
#            ax.set_xticklabels(range(0,25,6), fontsize=fsize)
#            ax.tick_params(axis='both', which='major', labelsize=fsize)
##            ax.set_yticklabels(ax.get_yticklabels(), fontsize=fsize)
#            plt.tight_layout()
#            fig.savefig(scen+'-Feat'+metric+'_Net'+net[-1]+'.pdf',format='pdf', dpi=100)
#            plt.close(fig)



##----Colored graphs set up
#        dfWeights = pd.DataFrame(df['index'])
#        if metric == 'age':
#            medians = dfDiff.iloc[:,0]
#        elif metric == 'flow':
#            medians = delta_peakflow
#        else:
#            # for 'headloss' and 'waterloss'
#            medians = dfDiff.sum(axis=1)
#            medians = medians * 30 #loss per day
#        dfWeights['Med'] = medians
#        dfWeights = dfWeights.sort_values(by='index')
#        dfWeights = dfWeights.reset_index(drop=True)
#        meds = dfWeights['Med']
#        weightValues = []
#        if features == 'pipes':
#            i = 0
#            for edge in list(l):
#                if l[edge].link_type==1:
#                    weightValues.append(meds[i])
#                    i += 1
#                else:
#                    weightValues.append(0)
#        elif features == 'junctions':
#            i = 0
#            for node in list(n):
#                if node in junctions:
#                    weightValues.append(meds[i])
#                    i += 1
#                else:
#                    weightValues.append(0)
#        else:
#            energyNodes = [node for node in list(n) if n[node].node_type==0]
#            i = 0
#            for node in list(n):
#                if node in energyNodes:
#                    weightValues.append(meds[i])
#                    i += 1
#                else:
#                    weightValues.append(0)
#        G, pos, colored, weights = coloredEdges(net, weightValues, features)
#
#        cut1, cut2, dataNew = jp.midPercentile2(weightValues,percent,"")
#
#        sources = [node for node in list(n) if n[node].node_type!=0]
#
#        if net == 'ky8':
#            sources.remove(1326)
#            G.remove_node(1326)
#            G.remove_node(1318)
#            G.remove_node(1322)
#            if metric != 'flow':
#                colored = tuple(feat for feat in colored if feat not in [1326,1318,1322])
#            else:
#                colored = tuple(feat for feat in colored if feat not in [(1322,1326),(1318,1322),(1318,1327)])
#
##----Colored node graph
#        if metric != 'flow':
#            fig, ax = plt.subplots(figsize=[6,6])
#            plt.axis('equal')
#            # base
#            nx.draw(G,pos, node_size=0, node_color='k', edge_color='C7', width=0.5)
#
#            # for sizing nodes by weight
#            if features != 'pipes':
#                nodesPos = []
#                nodesNeg = []
#                nodesPosThick = []
#                nodesNegThick = []
#                for feat in colored:
#                    # positive and in upper 2.5%
#                    if G.nodes[feat]['weight'] > 0 and G.nodes[feat]['weight'] >= cut1:
#                        nodesPosThick.append(feat)
#                    # just positive
#                    elif G.nodes[feat]['weight'] > 0:
#                        nodesPos.append(feat)
#                    # negative or zero and in lower 2.5%
#                    elif G.nodes[feat]['weight'] <= 0 and G.nodes[feat]['weight'] <= cut2:
#                        nodesNegThick.append(feat)
#                    # just negative
#                    elif G.nodes[feat]['weight'] <= 0:
#                        nodesNeg.append(feat)
#                # negative nodes, normal size
#                nx.draw(G,pos, nodelist=nodesNeg, edgelist=[], width=0.5, \
#                        node_size=2, node_color='C0', alpha=0.75)
#                # positive nodes, normal size
#                nx.draw(G,pos, nodelist=nodesPos, edgelist=[], width=0.5, \
#                        node_size=2, node_color='C3', alpha=0.75)
#                # negative nodes, larger
#                nx.draw(G,pos, nodelist=nodesNegThick, edgelist=[], width=0.5, \
#                        node_size=50, node_color='C0', alpha=0.75, label=r'$x\leq$'+str(round(cut2,1))+unit)
#                # positive nodes, larger
#                nx.draw(G,pos, nodelist=nodesPosThick, edgelist=[], width=0.5, \
#                        node_size=50, node_color='C3', alpha=0.75, label=str(round(cut1,1))+r'$x\geq$'+unit)
#            # source nodes
#            nx.draw(G,pos, nodelist=sources, edgelist=[], width=0.5, \
#                    node_size=35, node_color='k', node_shape='s', alpha=0.75)
#            ax.legend(frameon=False,loc=9,bbox_to_anchor=(0.5,0.05),ncol=2)
#            plt.tight_layout()
#            fig.savefig(net + metric + new + '_colorGraph.pdf', format='pdf', dpi=300)
#
##            fig = plt.figure(figsize=[6,6])
##            nodeColor = 'C0'
##            nodeSize = 0
##            nodeShape = '^'
##            lineWidth = 1
##            width = 1.5
##            alpha = 1
##            nx.draw(G,pos,node_size=nodeSize,edge_color=weights,width=width,alpha=alpha,\
##                    edge_cmap=plt.cm.RdBu, vmin=limitMin, vmax=limitMax)
##            sm = plt.cm.ScalarMappable(cmap=plt.cm.RdBu, norm=plt.Normalize(vmin=limitMin, vmax=limitMax))
##            sm._A = []
##            plt.colorbar(sm)
##            plt.axis('off')
##
##            # save to .png
##            fig.savefig(net + 'colored.png', format='png', dpi=300)
#
##-------Colored edges graph
#        else:
#            # for 'flow' only:
#            fig, ax = plt.subplots(figsize=[6,6])
#            plt.axis('equal')
#            # base
#            nx.draw(G,pos, node_size=0, node_color='k', edge_color='C7', width=0.5)
#
#            # for sizing edges by weights:
#            if features == 'pipes':
#                Pos = []
#                Neg = []
#                PosThick = []
#                NegThick = []
#                for feat in colored:
#                    # positive and in upper 2.5%
#                    if G.edges[feat]['weight'] > 0 and G.edges[feat]['weight'] >= cut1:
#                        PosThick.append(feat)
#                    # just positive
#                    elif G.edges[feat]['weight'] > 0:
#                        Pos.append(feat)
#                    # negative or zero and in lower 2.5%
#                    elif G.edges[feat]['weight'] <= 0 and G.edges[feat]['weight'] <= cut2:
#                        NegThick.append(feat)
#                    # just negative
#                    elif G.edges[feat]['weight'] <= 0:
#                        Neg.append(feat)
#                # negative, normal size
#                nx.draw(G,pos, edgelist=Neg, width=0.5, \
#                        node_size=0, edge_color='C0', alpha=0.75)
#                # positive, normal size
#                nx.draw(G,pos, edgelist=Pos, width=0.5, \
#                        node_size=0, edge_color='C3', alpha=0.75)
#                # negative, larger
#                nx.draw(G,pos, edgelist=NegThick, width=4, \
#                        node_size=0, edge_color='C0', alpha=0.75)
#                # positive, larger
#                nx.draw(G,pos, edgelist=PosThick, width=4, \
#                        node_size=0, edge_color='C3', alpha=0.75)
#            # source nodes
#            nx.draw(G,pos, nodelist=sources, edgelist=[], width=0.5, \
#                    node_size=35, node_color='k', node_shape='s', alpha=0.75)
#            legend_elements = [Line2D([0],[0],color='C0',lw=4,label=r'$x\leq$'+str(round(cut2,1))+unit), \
#                               Line2D([0],[0],color='C3',lw=4,label=str(round(cut1,1))+r'$x\geq$'+unit)]
#            ax.legend(handles=legend_elements, \
#                      frameon=False,loc=9,bbox_to_anchor=(0.5,0.00),ncol=2)
#            plt.tight_layout()
#            fig.savefig(net + metric + new + '_colorGraph.pdf', format='pdf', dpi=300)
#
"""
#---- COMPARATIVE HISTOGRAMS FOR BENCHMARK NETWORK ----
    # Plot properties - general
    fontsize = 16
    if scenario == "Reduc":
        scen = '1'
        yticks = np.arange(-.6,1.0,.2)
        yticklabels = [0.6,0.4,0.2,0,0.2,0.4,0.6,0.8]
    else:
        scen = '2'
        yticks = np.arange(-.6,1.0,.2)
        yticklabels = [0.6,0.4,0.2,0,0.2,0.4,0.6,0.8]

    # "Zoom" in on histogram
    def zoom(x, percent):
        # absolute value
        x = abs(x)
        # find upper tail cutoff
        cutoff = np.percentile(x,percent)
        # cut (don't show) bins after that point in plot code
        return cutoff

    # Get values from histogram
    def compHisto(x, percent):
        # if don't want zoom, set percent = 100
        bins = np.linspace(0,zoom(x,percent),int(jp.Sturges(abs(x))))
        xP = [i for i in x if i >= 0]
        xN = [abs(i) for i in x if i < 0]
        weight = 1.0/len(x)
        weights_xP = np.ones_like(xP) * weight
        weights_xN = np.ones_like(xN) * weight
        binVal_xP = ax.hist(xP, bins=bins, weights=weights_xP)
        binVal_xN = ax.hist(xN, bins=bins, weights=weights_xN)

        return bins, binVal_xP, binVal_xN

    # Plot modified comparative histogram = a bar chart
    alpha1 = 99 # cutoff so we have a zoomed-in histo, for wloss and age
    alpha2 = 99 # for energy loss and peak flow

    # Water Loss
    x = delta_nLeak
    bins, binVal_xP, binVal_xN = compHisto(x,alpha1)
    fig, ax = plt.subplots(figsize=[5,5])
    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
    widthsBar = np.ediff1d(bins)[0]
    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
    ax.set_xlabel(r'$\Delta$ Water Loss (gpm)', fontsize=fontsize)
    ax.set_ylabel('Frequency', fontsize=fontsize)
    ax.set_xticks(bins[0:-1:2])
    ax.set_xlim(bins[0],bins[-1])
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    plt.tight_layout()
    fig.savefig(scen+'-'+'WLossHisto_Net'+net[-1]+'.pdf', format='pdf', dpi=300)
    plt.close(fig)

    # Water Age
    x = delta_age24hr
    bins, binVal_xP, binVal_xN = compHisto(x, alpha1)
    fig, ax = plt.subplots(figsize=[5,5])
    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
    widthsBar = np.ediff1d(bins)[0]
    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
    ax.set_xlabel(r'$\Delta$ Water Age (hr)', fontsize=fontsize)
    ax.set_ylabel('Frequency', fontsize=fontsize)
    ax.set_xticks(bins[0:-1:2])
    ax.set_xlim(bins[0],bins[-1])
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f')) # or %d
    plt.tight_layout()
    fig.savefig(scen+'-'+'AgeHisto_Net'+net[-1]+'.pdf', format='pdf', dpi=300)
    plt.close(fig)

    # Energy Loss
    x = delta_nloss
    bins, binVal_xP, binVal_xN = compHisto(x, alpha2)
    fig, ax = plt.subplots(figsize=[5,5])
    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
    widthsBar = np.ediff1d(bins)[0]
    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
    ax.set_xlabel(r'$\Delta$ Energy Loss (ft)', fontsize=fontsize)
    ax.set_ylabel('Frequency', fontsize=fontsize)
    ax.set_xticks(bins[0:-1:2])
    ax.set_xlim(bins[0],bins[-1])
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    plt.tight_layout()
    fig.savefig(scen+'-'+'EnergyHisto_Net'+net[-1]+'.pdf', format='pdf', dpi=300)
    plt.close(fig)

    # Peak Flow
    x = delta_peakflow
    bins, binVal_xP, binVal_xN = compHisto(x, alpha2)
    fig, ax = plt.subplots(figsize=[5,5])
    plt.axhline(0, color='C7', linewidth=0.25, alpha = 0.75)
    widthsBar = np.ediff1d(bins)[0]
    ax.bar(bins[0:-1], -1*binVal_xP[0], align='edge', width=widthsBar, color='C3', edgecolor='white', linewidth=0.25)
    ax.bar(bins[0:-1], binVal_xN[0], align='edge', width=widthsBar, color='C0', edgecolor='white', linewidth=0.25)
    ax.set_xlabel(r'$\Delta$ Peak Flow (gpm)', fontsize=fontsize)
    ax.set_ylabel('Frequency', fontsize=fontsize)
    ax.set_xticks(bins[0:-1:2])
    ax.set_xlim(bins[0],bins[-1])
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    plt.tight_layout()
    fig.savefig(scen+'-'+'PkFlowHisto_Net'+net[-1]+'.pdf', format='pdf', dpi=300)
    plt.close(fig)
"""