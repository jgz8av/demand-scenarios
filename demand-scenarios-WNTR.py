# -*- coding: utf-8 -*-
"""
demand scenarios project - pressure driven analysis
Author: Janice Zhuang
Python 2.7, WNTR 0.1.7

NEED TO UPDATE UNITS
"""
# for data organization and calculation
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# for WDS analysis
import wntr

#------------------#
#-----INPUTS-------#
#------------------#

#networks = ['ky3_v1', 'ky5_v2', 'ky6_v1', 'ky7', 'ky1_v1', 'ky8_v2', 'ky4']
# RUNTIME ERROR: ky3_v1, ky5_v2, ky8_v2

#networks = ['ky6_v1_pat1', 'ky6_v1_pat3', 'ky7_pat1', 'ky7_pat3', 'ky1_v1_pat1', 'ky1_v1_pat3', 'ky4_pat1', 'ky4_pat3']
networks_S0 = ['ky6_v1_pat1', 'ky7_pat1','ky1_v1_pat1', 'ky4_pat1']
networks_S1 = ['ky6_v1_pat3', 'ky7_pat3', 'ky1_v1_pat3', 'ky4_pat3']

networks_S0 = ['ky4_pat1']
networks_S1 = ['ky4_pat3']

scenario = 'Base' # S0
#scenario = 'Cons' # S1

if scenario == 'Base':
    networks = networks_S0
elif scenario == 'Cons':
    networks = networks_S1
    
for net in networks:

    print "WNTR PDA -", net

    #----------------#
    #-----SET UP-----#
    #----------------#
    
    # Create a water network model
    inp_file = 'networks/WNTR/'+net+'.inp'
    wn = wntr.network.WaterNetworkModel(inp_file)
    
    # Set time related options
    wn.options.time.duration = 24*3600  # duration = 24hr
    wn.options.time.hydraulic_timestep = 3600/2  # hydraulic timestep = 30min
    wn.options.time.quality_timestep = 3600/12   # quality timestep = 5min
    wn.options.time.pattern_timestep = 3600    # pattern timestep = 1hr
    wn.options.time.report_timestep = 3600/2    # reporting timestep = 30min
    
    ## Set quality options - only for EPANET Simulator
    #wn.options.quality.mode = 'Age'
    #wn.options.quality.q_units = 's'
    
    # Solver options = same as default
    
    # Set pattern
    if scenario == 'Base':
        pat = '1'
    elif scenario == 'Cons':
        pat = '3'
    wn.options.hydraulic.pattern = pat
    
    #-------------------#
    #-----ADD LEAKS-----#
    #-------------------#
    
    # Get element lists
    res_list = wn.reservoir_name_list
    tank_list = wn.tank_name_list
    junc_list = wn.junction_name_list
    pipe_list = wn.pipe_name_list
    pump_list = wn.pump_name_list
    valve_list = wn.valve_name_list
    
    # Loop through nodes to add to each node
    # Constants:
        # Discharge coeff alpha = 0.75 = (default)
        # Area = 3e-5 (m^2) = 1mm diameter hole
        # Start time = 0
        # End time = 86400
    for junction in junc_list:
            alpha = 0.75
            area = 7.9e-7
            start_time = 0*3600
            end_time = 24*3600
            wn.get_node(junction).add_leak(wn, area=area, start_time=start_time, end_time=end_time)
    
    #------------------#
    #-----SIMULATE-----#
    #------------------# 
        
    # Simulate hydraulics - pressure driven demand analysis
    sim = wntr.sim.WNTRSimulator(wn, mode='PDD')
#    sim = wntr.sim.WNTRSimulator(wn)
    
    # Checks:
    # Print pattern
    pattern = wn.get_pattern(pat).multipliers    # array
    # Plot pattern
    #fig = plt.figure(figsize=(6,3))
    #plt.plot(pattern_S0)
    #plt.title('S0')
    # Print # nodes, # links
    print "# Nodes:", len(wn.node_name_list)
    print "# Links:", len(wn.link_name_list)
    

    
    #---------------------------#
    #-----MINE WNTR RESULTS-----#
    #---------------------------#
    
    # Unit conversions:
    # 1 m^3/s = 15,850.32 gpm
    # 1 m = 3.28 ft
    # 1 m of head = 1.42197 psi
    
    # Get simulation results
    results_S0 = sim.run_sim()
    
    # Get dataframes (master) of results 
    # Age - NA for WNTRSimulator
    # Head Loss - NA for WNTRSimulator
    # Water Loss - bug = can't get non-zero results for WNTRSimulator
    leaks_df = results_S0.node['leak_demand'].loc[:,:]
    leaks_df = leaks_df.multiply(15850.32) # conversion to gpm from m3/s
    leaks_demands_df = leaks_df
    leaks_demands_df['Total'] = leaks_demands_df.sum(axis=1) # [gpm]
    total_leaks = leaks_demands_df['Total'] # [gpm]
    # Flows
    flows_df = results_S0.link['flowrate'].loc[:,:] # dataframe [timestep x links]
    flows_df = flows_df.multiply(15850.32) # conversion to gpm from m3/s
    flows_df['Peak'] = flows_df.max(axis=1) # [gpm]
    peak_flows = flows_df['Peak']   # [gpm]
    # Demands
    node_demands_df = results_S0.node['demand'].loc[:,:] # [m^3/s]
    node_demands_df = node_demands_df.multiply(15850.32)  # [gpm]
    node_demands_df['Total'] = node_demands_df.sum(axis=1)
    total_demands = node_demands_df['Total'] # [gpm]
    # Heads
    node_heads_df = results_S0.node['head'].loc[:,:] # [m]
    node_heads_df = node_heads_df.multiply(3.28) # [ft]
    # Pressures
    node_pressures_df = results_S0.node['pressure'].loc[:,:] # [m]
    node_pressures_df = node_pressures_df.multiply(1.42197)  # [psi]
    
    # Filter into element-specific dataframes of results
    res_demands_df = node_demands_df.filter(items=res_list)
    tank_demands_df = node_demands_df.filter(items=tank_list)
    junc_demands_df = node_demands_df.filter(items=junc_list)
    
    res_heads_df = node_heads_df.filter(items=res_list)
    tank_heads_df = node_heads_df.filter(items=tank_list)
    junc_heads_df = node_heads_df.filter(items=junc_list)
    
    junc_pressures_df = node_pressures_df.filter(items=junc_list)
    junc_leaks_df = leaks_df.filter(items=junc_list)
    
    pipe_flows_df = flows_df.filter(items=pipe_list)
    pump_flows_df = flows_df.filter(items=pump_list)
    valve_flows_df = flows_df.filter(items=valve_list)
    
    #---------------------------#
    #-----CALCULATE RESULTS-----#
    #---------------------------#
    
    # Energy - Head*Demand [gpm*ft]
    def node_source_user_Hd_results(node_demands_df, node_heads_df, element_list):
        # Filter dataframe to get results of specific elements
        # Split element's results into source (neg dem) or user (pos dem)
        # get demands
        element_demands_df = node_demands_df.filter(items=element_list) 
        # get heads
        element_heads_df = node_heads_df.filter(items=element_list)
        # get heads*demands
        element_Hd_df = element_heads_df.multiply(element_demands_df)
        # split into source or user dataframes
        # source = negative dem
        source_Hd_df = element_Hd_df[element_Hd_df <=0].fillna(0)
        # user = positive dem
        user_Hd_df = element_Hd_df[element_Hd_df >=0].fillna(0)
        return source_Hd_df, user_Hd_df
    
    res_source_Hd_df, res_fill_Hd_df = \
    node_source_user_Hd_results(node_demands_df,node_heads_df,res_list) 
    tank_source_Hd_df, tank_user_Hd_df = \
    node_source_user_Hd_results(node_demands_df,node_heads_df,tank_list)
    junc_dummy_Hd_df, junc_user_Hd_df = \
    node_source_user_Hd_results(node_demands_df,node_heads_df,junc_list)
    
#    leak_user_Hd_df = junc_leaks_df.multiply(junc_heads_df)
    leak_source_Hd_df, leak_user_Hd_df = \
    node_source_user_Hd_results(leaks_df,node_heads_df,junc_list)
    
    res_source_sum = res_source_Hd_df.sum(axis=0).sum(axis=0)
    tank_source_sum = tank_source_Hd_df.sum(axis=0).sum(axis=0)
    tank_user_sum = tank_user_Hd_df.sum(axis=0).sum(axis=0)
    junc_user_sum = junc_user_Hd_df.sum(axis=0).sum(axis=0)
    leak_user_sum = leak_user_Hd_df.sum(axis=0).sum(axis=0)
            
    # Energy - Headloss * Flow [gpm*ft]
    def headloss_results(link_list, node_heads_df, wn):
        # Returns headlosses [ft] for links by taking absolute difference between nodal heads
        frames = []
        if len(link_list) != 0:
            # For link in links list:
            for link in link_list:
                # Get start node
                start_node = wn.get_link(link).start_node_name
                # Get end node
                end_node = wn.get_link(link).end_node_name
                # Get the df of heads for the start node, end node
                adjacent_nodes = [start_node, end_node]
                adj_node_heads_df = node_heads_df.filter(items=adjacent_nodes) # [ft]
                # Take the difference between the heads = headloss at link
                heads_diff = adj_node_heads_df.iloc[:,0] - adj_node_heads_df.iloc[:,-1]
                # Get the absolute value of those
                abs_heads_diff = heads_diff.abs()
                # Append results of this link
                frames.append(abs_heads_diff)
            # Create dataframe of headlosses
            headloss_df = pd.concat(frames, axis=1)
            headloss_df.columns = link_list
        else:
            # dummy dataframe
            headloss_df = pd.DataFrame(0, index=np.arange(0,24*3600,1800), columns=[0,1])
        return headloss_df
    
    pipe_headloss_df = headloss_results(pipe_list, node_heads_df, wn)
    pump_headloss_df = headloss_results(pump_list, node_heads_df, wn)
    #calculating valves would need to revise function, but since no vavles in ky4, hold for now
    valve_headloss_df = headloss_results(valve_list, node_heads_df, wn)
    
    def link_source_user_hlQ_results(flows_df, node_heads_df, wn, element_list):
        # Filter dataframe to get results of specific elements
        # get abs(flows)
        element_flows_df = flows_df.filter(items=element_list)
        abs_element_flows_df = element_flows_df.abs()
        # get headlosses
        element_headloss_df = headloss_results(element_list, node_heads_df, wn)
        # get headlosses*flows
        element_hlQ_df = abs_element_flows_df.multiply(element_headloss_df)
        return element_hlQ_df
    
    pump_source_hlQ_df = \
    link_source_user_hlQ_results(flows_df, node_heads_df, wn, pump_list).multiply(-1) # should be neg bc source
    pipe_user_hlQ_df = \
    link_source_user_hlQ_results(flows_df, node_heads_df, wn, pipe_list) # should be pos bc user
    valve_user_hlQ_df = \
    link_source_user_hlQ_results(flows_df, node_heads_df, wn, valve_list)
    
    pump_source_sum = pump_source_hlQ_df.sum(axis=0).sum(axis=0)
    pipe_user_sum = pipe_user_hlQ_df.sum(axis=0).sum(axis=0)
    valve_user_sum = valve_user_hlQ_df.sum(axis=0).sum(axis=0)
    
    Pin = res_source_sum + pump_source_sum + tank_source_sum
    Pout = junc_user_sum + tank_user_sum + leak_user_sum
    Ploss = pipe_user_sum + valve_user_sum
    Perror = Pin + Pout + Ploss
    
    
    # Water Loss = Background Leakage 
    # (mid 95% for water loss metric and 100% for energy breakdown)
    
    total_leaks_sum = sum(total_leaks)
    junc_demands_sum = sum(junc_demands_df.sum(axis=1))
    tank_demands_sum = sum(tank_demands_df[tank_demands_df >=0].fillna(0).sum(axis=1))
    total_demand_sum = junc_demands_sum + tank_demands_sum
    
    def leak_results():
        # For node (node i) in junctions list:
            # Get connecting link:
            # For link in connecting links:
                # Get node at other end of link (node j)
                # Get pressures for node i and node j
                # Get pressures of this link (100%) = take average of nodal pressures
                # Get mid 95% of pressure values (for water loss metric)
                # Calculate leakage at this link = B*length*linkPress**a
                    # 1ft = 0.3048m, 1.42197psi = 1m of head, 15.8503gpm = 1L/s
                # Calculate mide 95% leakage
                # Sum and add to lists of leak value for all connecting links
            # Calculate leakage at this node = 0.5*sum(all connecting link leakages)
            # Calculate mid 95% leakage at this node
            # Add to list of leak values for all junctions
                
        return ""
        
    #---------------------------#
    #-----PRINT-----#
    #---------------------------#
    
    print res_source_sum
    print pump_source_sum
    print tank_source_sum
    print junc_user_sum
    print tank_user_sum
    print leak_user_sum
    print pipe_user_sum
    print ""
    print Perror
    print ""
    print "Total leaks (gpm)=", total_leaks_sum

#    #------------------------#
#    #-----WRITE TO EXCEL-----#
#    #------------------------#
#    
#    # Sheet 1 = by time - flows, leaks, demands, heads
#    #
#    # by time
#    # sum of the Series
#    
#    # Results Summary Table - Excel
#    df_filename = "EnergyBreakdown_PDD_"+net[:-5]+".xlsx"
#    if scenario == 'Base':
#        df_energyS0 = pd.DataFrame({'aTime_0.5hr':range(0,49), \
#                             'bReservoirs_S0 (gpm*ft)':res_source_Hd_df.sum(axis=1), 'cPumps_S0':pump_source_hlQ_df.sum(axis=1), 'dTanksSource_S0':tank_source_Hd_df.sum(axis=1), \
#                             'eJunctions_S0':junc_user_Hd_df.sum(axis=1), 'fTanksUser_S0':tank_user_Hd_df.sum(axis=1), 'gLeaksUser_S0':leak_user_Hd_df.sum(axis=1), \
#                             'hPipe Headloss_S0':pipe_user_hlQ_df.sum(axis=1), 'iValve Headloss_S0':valve_user_hlQ_df.sum(axis=1)})
#        df_excel = df_energyS0
#        df_excel.to_excel(df_filename, sheet_name='Sheet1', index=False)
#    
#    elif scenario == 'Cons':
#        df_energyS1 = pd.DataFrame({'aTime_0.5hr':range(0,49), \
#                             'bReservoirs_S1 (gpm*ft)':res_source_Hd_df.sum(axis=1), 'cPumps_S1':pump_source_hlQ_df.sum(axis=1), 'dTanksSource_S1':tank_source_Hd_df.sum(axis=1), \
#                             'eJunctions_S1':junc_user_Hd_df.sum(axis=1), 'fTanksUser_S1':tank_user_Hd_df.sum(axis=1), 'gLeaksUser_S1':leak_user_Hd_df.sum(axis=1), \
#                             'hPipe Headloss_S1':pipe_user_hlQ_df.sum(axis=1), 'iValve Headloss_S1':valve_user_hlQ_df.sum(axis=1)})
#    
#        dfRead = pd.read_excel(df_filename)
#        frames = [dfRead, df_energyS1.reset_index(drop=True)]
#        df_excel = pd.concat(frames,axis=1)
#        writer = pd.ExcelWriter(df_filename)
#        df_excel.to_excel(writer, sheet_name='Sheet 1', index=False)
#        writer.save()

