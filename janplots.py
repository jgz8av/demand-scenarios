"""
Functions for Demand Management project, specifically plots
Author: Janice Zhuang
Python 2.7
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter, ScalarFormatter
import plotly.plotly as py
import plotly.graph_objs as go
import seaborn as sns
import janepanet as jep # Janice's own functions

#-----PLOTS GENERAL
def Sturges(data):
    # Sturges method of defining #classes for histogram
    from math import log10
    n = len(data)
    m = 1 + 3.3*log10(n)

    # return #classes rounded to nearest whole#
    return round(m,0)

def pctDiff(xBase, xNew):
    # Returns delta New - Base
    # Returns percent difference (New - Base) / Base
    # xBase = list of values for a parameter for baseline scenario
    # xNew = for new scenario
    # negative value = improvement
    # positive value = worsen
    deltaBF = np.array(xNew) - np.array(xBase)
    i = 0
    pctDiffBF = []
    for value in xBase:
        # to avoid divide by 0 error...
        if value==0 and xBase[i]==0:
            pctDiffBF.append(value)
        else:
            pctDiffBF.append(100 * np.divide(deltaBF[i], np.array(value)))
        i += 1
    return deltaBF, pctDiffBF

def norm(x):
    # Returns normalized data
    normListP = []
    normListN = []
    # x = delta list of parameter
    xPos = [xi for xi in x if xi >=0]   # positive values (increased)
    xNeg = [abs(xi) for xi in x if xi < 0]  # negative values (decreased)
    for xi in xPos:
        norm = (xi - min(xPos))/(max(xPos) - min(xPos))
        normListP.append(norm)
    for xi in xNeg:
        norm = (xi - min(xNeg))/(max(xNeg) - min(xNeg))
        normListN.append(norm)
    return normListP, normListN

def weib(x,n,a):
    # Returns weibull distribution of shape parameters a,n
    return (a/n)*(x/n)**(a-1)*np.exp(-(x/n)**a)

#-----PLOTS FOR RESULTS 1.0

def mlabLine(x,y,xlabel,ylabel,title,saveas):
    # Creates matplotlib line plot
    figure = plt.figure()
    plt.plot(x,y)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()
    figure.savefig(saveas + '.pdf', format='pdf', dpi=600)
    plt.close(figure)

def plotlyLine(x,y,xlabel,ylabel,title):
    # Creates plotly ONLINE line plot
    xy = go.Scatter(
            x = x,
            y = y
            )
    data = [xy]
    layout = dict(title=title,
                  xaxis = dict(title=xlabel),
                  yaxis = dict(title=ylabel)
                  )
    figurePLY = dict(data=data, layout=layout)
    py.iplot(figurePLY, filename=title)

def histoSubPlotFold(x,ax,quantile,alpha,title,xlabel,ylabel,deci):
    # Creates histogram subplot folded (overlaying pos & neg values)
    mu = x.mean()
    median = np.median(x)
    sigma = x.std()
    if "Normalized" in xlabel:
        xP, xN = norm(x)
    else:
        xP = [i for i in x if i >= 0]
        xN = [abs(i) for i in x if i < 0]
    quant = quantile
    cutoff = np.percentile(np.array(xP+xN), quant)
    bins = np.linspace(0, cutoff, 20)
    weights = np.ones_like(x)/len(x)
    weightsP = []
    weightsN = []
    count = 0
    for i in x:
        if i >= 0:
            weightsP.append(weights[count])
        else:
            weightsN.append(weights[count])
#    bVP = ax.hist(xP, bins=bins, weights=weightsP, alpha=alpha, \
#                      color='C3', label='increase')
#    bVN = ax.hist(xN, bins=bins, weights=weightsN, alpha=alpha, \
#                      color='C0', label='decrease')
    if len(xP) > len(xN):
        bVP = ax.hist(xP, bins=bins, weights=weightsP, alpha=alpha, \
                      color='C3', label='increase')
        bVN = ax.hist(xN, bins=bins, weights=weightsN, alpha=alpha, \
                      color='C0', label='decrease')
    else:
        bVN = ax.hist(xN, bins=bins, weights=weightsN, alpha=alpha, \
                      color='C0', label='decrease')
        bVP = ax.hist(xP, bins=bins, weights=weightsP, alpha=alpha, \
                      color='C3', label='increase')
    ax.set_title(title)
    ax.set_xlabel(xlabel, fontsize=11)
    ax.set_ylabel(ylabel, fontsize=11)
    ax.set_ylim([0,1])
    ax.set_xlim([0,cutoff])
    labels = np.array([round(i,4) for i in bVP[1]])
    ax.set_xticks(labels)
    ax.set_xticklabels(labels, rotation=0, fontsize=9)
    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    ax.xaxis.set_major_formatter(FormatStrFormatter(deci))
    handles, leglabels = ax.get_legend_handles_labels()
    if leglabels[0]=='increase':
        ax.legend(handles[::-1], leglabels[::-1], frameon=False, fontsize=11)
    else:
        ax.legend(handles, leglabels, frameon=False, fontsize=11)
    props = dict(boxstyle='round',facecolor='white',edgecolor='none',alpha=0.5)
    textstr = '$\mu=%.1f$\n$\mathrm{med}=%.1f$\n$\sigma=%.1f$'%(mu, \
                                   median,sigma)
    ax.text(0.03,0.98,textstr,transform=ax.transAxes,fontsize=11, \
            va='top',bbox=props)

def histoSubPlotWhole(x,ax,quantile,title,xlabel,ylabel,deci):
    # Creates histogram subplot unfolded (normal)
    mu = round(x.mean(),1)
    median = round(np.median(x),1)
    sigma = round(x.std(),1)
    quant = quantile
    qa = int(round((100-quant)/2)) + quantile
    qb = 100 - qa
    cutoffa = np.percentile(x, qa)
    cutoffb = np.percentile(x, qb)
    bins = np.linspace(cutoffb, cutoffa, 20)
    weights = np.ones_like(x)/len(x)
    bV = ax.hist(x, bins=bins, weights=weights, alpha=1, color='C1')
#    bV = ax.hist(x, bins=20, alpha=1, normed=True)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_ylim([0,1])
    labels = np.array([round(i,4) for i in bV[1]])
    ax.set_xticks(labels)
    ax.set_xticklabels(labels, rotation=70, fontsize=9)
    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    ax.xaxis.set_major_formatter(FormatStrFormatter(deci))
    props = dict(boxstyle='round',facecolor='white',edgecolor='none',alpha=0.5)
    textstr = '$\mu=%.1f$\n$\mathrm{med}=%.1f$\n$\sigma=%.1f$'%(mu, \
                                   median,sigma)
    ax.text(0.03,0.98,textstr,transform=ax.transAxes,fontsize=11, \
            va='top',bbox=props)

def comboPDF(pct_list_list,quantile,bandwidth,kdecut, \
             title,xlabel,ylabel,saveas):
    # Creates overlaying PDFs of all networks
    figure = plt.figure(figsize=(12,12))
    i = 0
    cut = []
    for ilist in pct_list_list:
        data = np.array(sorted(ilist))
        cut.append(np.percentile(data, quantile))
        sns.kdeplot(data, bw=bandwidth, label=networks[i], cut=kdecut)
        i += 1
    sns.set_style("whitegrid")
    plt.legend()
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    if "Leak" in title:
        cutoff = min(cut)
        plt.xlim(left=cutoff)
    else:
        cutoff = round(sum(cut)/len(cut),0)
        plt.xlim([-100,cutoff])
    figure.savefig(saveas + '.pdf', format='pdf', dpi=600)
    plt.close(figure)

#-----PLOTS FOR RESULTS 2.0

def df4MatrixPlot_all(features,prop,ts_index,n,l,metric,patternName,net):
    # Create dataframe for matrix plot/heatmap
    # Dataframe includes values for all features @ all times
    # Creates .csv from full dataframe
    # Returns values-only dataframe and array

    # pipes or nodes?
    if features == "Junctions":
        featureList = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]
        featureNames = [n[feature].id for feature in featureList]
    elif features == "Pipes":
        featureList = [pipe for pipe in list(l) \
                                if l[pipe].link_type==1]
        featureNames = [l[feature].id for feature in featureList]
    else:
        featureList = list(n)
        featureNames = [n[feature].id for feature in featureList]
    # get all values
    propArray = []
    for feature in featureList:
        # pipes or nodes?
        if features == "Pipes":
            row = jep.getLinkValues(feature,prop,ts_index,l)
        else:
            row = jep.getNodeValues(feature,prop, ts_index,n)
        propArray.append(row)
    # convert to dataframe, for ease
    df = pd.DataFrame(propArray)
    df.columns = range(0,len(ts_index))
    df['Index'] = featureList
    df['Name'] = featureNames
    # write matrix to csv
    # new = name of pattern used
#    df.to_csv(net + str(metric) + patternName + '.csv', index=False)
    # create dataframe of values-only
    dfStrip = df[df.columns.difference(['Index','Name'])]

    return dfStrip, propArray

def noOutliers(propArray,metric):
    # Find outliers using boxplot
    # Plots histogram and boxplot, for visuals
    # Returns data cutoffs so there are no outliers

    # list of all values
    data = np.array(propArray).flatten('C')
#    # histogram of all values
#    plt.figure()
#    plt.hist(data)
#    plt.title("All Values Histogram")
#    plt.xlabel(str(metric))
    # boxplot of all values
#    plt.figure()
    boxDict = plt.boxplot(data)
#    plt.title("All Values Boxplot")
    # get data from boxplot
    #outliers = boxDict['fliers'][0].get_ydata()
    cap1 = boxDict['caps'][0].get_ydata()[0] # lower lim
    cap2 = boxDict['caps'][1].get_ydata()[0] # upper lim
    dataNew = [x for x in data if (x>=cap1 and x<=cap2)]
#    # histogram of new values
#    plt.figure()
#    plt.hist(dataNew)
#    plt.title("Values w/out Outliers Histogram")
#    plt.xlabel(str(metric))

    return cap1, cap2

def midPercentile(propArray,percent,metric):
    # Find middle percentage of data
    # Plots histograms, for visuals
    # Returns cutoffs and new array of central X% of data

    alpha = 100.0-float(percent)
    lower = alpha/2.0
    upper = 100.0-lower
    # list of all values
    data = np.array(propArray).flatten('C')
#    # histogram of all values
#    plt.figure()
#    plt.hist(data)
#    plt.title("All Values Histogram")
#    plt.xlabel(str(metric))
    # get percentiles of tails, to cut
    cut1 = np.percentile(data,lower) # lower lim
    cut2 = np.percentile(data,upper) # upper lim
    dataNew = [v for v in data if (v>=cut1 and v<=cut2)]
#    # histogram of new values
#    plt.figure()
#    plt.hist(dataNew)
#    plt.title("Mid-" + str(percent) + "% Values Histogram")
#    plt.xlabel(str(metric))

    return cut1, cut2, dataNew

def midPercentile2(propArray,percent,metric):
    # Finds %cutoff for absolute value of data
    # e.g. solves the lower? upper? two-tailed? question
    # Returns cutoffs and new array of central X% of data

    # list of all values
    data = np.array(propArray).flatten('C')
    # list of abs(all values), to find cuttoff
    dataAbs = abs(data)
    # get percentiles of upper tail (since only positive #s now), to cut
    cut = np.percentile(dataAbs,percent)
    cut1 = cut # upper limit
    cut2 = -cut # lower limit
    dataNew = [v for v in data if (v>=cut1 and v<=cut2)]

    return cut1, cut2, dataNew