# -*- coding: utf-8 -*-
"""
demand scenarios project - plotting response curves of random simulation results
Author: Janice Zhuang
Python 2.7, epanettools 0.0.9
"""
import pandas as pd
import matplotlib.pyplot as plt

# Read in the excel data
save_dir = 'results/figures/'
# file contains organized results from the random sampling simulations
df_filename = 'results/ResponseCurveData_EPANETky4_plot.xlsx'
df = pd.read_excel(df_filename, sheet_name='To_Plot')

# Set standard font sizes and colors and what not
fsize=14 # 14pt for 0.75 latex scale to be like 12pt

# 5 plots for 5 metrics...min, med, max
metrics = ['Water Loss', 'Water Age', 'Peak Flow', 'Energy Input', 'Energy Loss']

percents = df['%']

for metric in metrics:
    if metric == 'Water Age':
    # Metric: I_a, Age @ 24 hours [hr]
        df_age = df.iloc[:,1:4]
        df_max = df_age.iloc[:,0]
        df_med = df_age.iloc[:,1]
        df_min = df_age.iloc[:,2]
        unit = '(hr)'

    elif metric == 'Energy Input':
    # Metric: I_e-input, Energy input [kWh]
        df_energy_in = df.iloc[:,4:7]
        df_max = df_energy_in.iloc[:,0]
        df_med = df_energy_in.iloc[:,1]
        df_min = df_energy_in.iloc[:,2]
        unit = '(kWh)'

    elif metric == 'Energy Loss':
    # Metric: I_e-loss, Energy loss [kWh]
        df_energy_loss = df.iloc[:,7:10]
        df_max = df_energy_loss.iloc[:,0]
        df_med = df_energy_loss.iloc[:,1]
        df_min = df_energy_loss.iloc[:,2]
        unit = '(kWh)'

    elif metric == 'Peak Flow':
    # Metric: I_f, Peak flow [LPS]
        df_flow = df.iloc[:,10:13]
        df_max = df_flow.iloc[:,0]
        df_med = df_flow.iloc[:,1]
        df_min = df_flow.iloc[:,2]
        unit = '(LPS)'

    elif metric == 'Water Loss':
    # Metric: I_w, Water Loss [MLD]
        df_leak = df.iloc[:,13:16]
        df_max = df_leak.iloc[:,0]
        df_med = df_leak.iloc[:,1]
        df_min = df_leak.iloc[:,2]
        unit = '(MLD)'


    # Plot min, med, max

    fig, ax = plt.subplots()

    ax.plot(percents, df_med, color='C0', markerfacecolor='none', linewidth=3)
    ax.fill_between(percents, df_max, df_min, facecolor='C7', alpha = 0.5)
    #ax.plot(percents, df_max, '--', color='C1', markerfacecolor='none', linewidth=1)
    #ax.plot(percents, df_min, '--', color='C1', markerfacecolor='none', linewidth=1)

    #plt.legend(['Med','Min to Max'],frameon=False,fontsize=fsize)

    ax.set_xlabel('Percent of S1 Nodes (%)',fontsize=fsize)
    ax.set_ylabel(metric + ' ' + unit,fontsize=fsize)
    ax.set_xlim(0,100)
    ax.tick_params(axis='both', which='major', labelsize=fsize)

    fig.tight_layout()

    fig.savefig(save_dir + 'ResponseCurve_' + metric + '.pdf', format='pdf', dpi=100)

