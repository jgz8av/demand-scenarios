# -*- coding: utf-8 -*-
"""
Created on Sat Jan 19 00:33:56 2019

@author: Janice
"""

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np

#----READ FILE
save_dir = 'results/figures/v2'
# file contains organized results from all networks
df_filename = 'results/All-Net_Info_95P4_v2.xlsx'
df = pd.read_excel(df_filename, sheet_name='Sheet1')
#print df.columns.values.tolist()
df = df.set_index('Rename')

patterns = ['S0','S1']

# Set standard font sizes and colors and what not
fsize=14 # 14pt for 0.75 latex scale to be like 12pt

# 5 plots for 5 metrics...min, med, max
metrics = ['Water Loss', 'Water Age', 'Peak Flow', 'Energy Input', 'Energy Loss']

pipe_lengths = df.iloc[:,1]

for metric in metrics:
    if metric == 'Water Age':
    # Metric: I_a, Age @ 24 hours [hr]
        df_S0 = df.iloc[:,6]
        df_S1 = df.iloc[:,11]
        unit = '(hr)'

    elif metric == 'Energy Input':
    # Metric: I_e-input, Energy input [kWh]
        df_S0 = df.iloc[:,8]
        df_S1 = df.iloc[:,13]
        unit = '(kWh)'

    elif metric == 'Energy Loss':
    # Metric: I_e-loss, Energy loss [kWh]
        df_S0 = df.iloc[:,9]
        df_S1 = df.iloc[:,14]
        unit = '(kWh)'

    elif metric == 'Peak Flow':
    # Metric: I_f, Peak flow [LPS]
        df_S0 = df.iloc[:,7]
        df_S1 = df.iloc[:,12]
        unit = '(LPS)'

    elif metric == 'Water Loss':
    # Metric: I_w, Water Loss [MLD]
        df_S0 = df.iloc[:,5]
        df_S1 = df.iloc[:,10]
        unit = '(MLD)'

    # Plot

    fig, ax = plt.subplots()

    ax.plot(pipe_lengths, df_S0, 'o', color='grey', markerfacecolor='grey', markersize=fsize, alpha=0.75)
    ax.plot(pipe_lengths, df_S1, 'o', color='C0', markerfacecolor='none', markersize=fsize)

    plt.legend(['S0','S1'],frameon=False,fontsize=fsize,loc=9,bbox_to_anchor=(0.5,-0.15),ncol=2)

    ax.set_xlabel('Pipe Length (km)',fontsize=fsize)
    ax.set_ylabel(metric + ' ' + unit,fontsize=fsize)
    ax.tick_params(axis='both', which='major', labelsize=fsize)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%d'))

    fig.tight_layout()

    fig.savefig(save_dir + 'PipeLength_' + metric + '.pdf', format='pdf', dpi=100)


