"""
demand scenarios project - randomly sample nodes
Author: Janice Zhuang
Python 2.7, epanettools 0.0.9
"""
import numpy as np
import pandas as pd
import random

import tempfile, os
import sys
from epanettools.epanettools import EPANetSimulation, Node, Link, Network, \
                                    Nodes, Links, Patterns, Pattern, \
                                    Controls, Control
import EPANETresults as er

#----------------#
#-----INPUTS-----#
#----------------#

# Get network
net = 'ky4'

# Get path for network
file = net + '.inp'
dir = 'networks'
path = os.path.join(os.getcwd(), dir, file)

# Set percentage
# Input via runner.sh script, run with bash for 0% to 100% due to issues with \
# epanettools' close functions not working properly?
percent = int(sys.argv[1])
#percent = 0 # trial run for calculating, storing, and recording metrics testing

#----------------------#
#-----SET DEFAULTS-----#
#----------------------#

# Network property variables
d = Node.value_type['EN_DEMAND']
p = Node.value_type['EN_PRESSURE']
hl = Link.value_type['EN_HEADLOSS']
en = Link.value_type['EN_ENERGY']
b = Node.value_type['EN_BASEDEMAND']
leng = Link.value_type['EN_LENGTH']
flow = Link.value_type['EN_FLOW']
q = Node.value_type['EN_QUALITY']
diam = Link.value_type['EN_DIAMETER']
H = Node.value_type['EN_HEAD']
pat = Node.value_type['EN_PATTERN'] # sets demand pattern
sp = Node.value_type['EN_SOURCEPAT'] # sets pattern for water quality sources

#---------------------#
#-----FOR STORAGE-----#
#---------------------#

# store results after each iteration in a list in order to get min, med, max points

max_age_results_list = []
energy_in_results_list = []
energy_loss_results_list = []
peak_flow_results_list = []
water_loss_results_list = []

#--------------------------------#
#-----RANDOM SAMPLING STARTS-----#
#--------------------------------#

# randomly sample set of nodes 30x...
for s in range(0,30):

    #--------------------------------#
    #-----INITAL SET UP: DEFAULT-----#
    #--------------------------------#

    # Set up network in S0 Base Scenario:

    # Open network file to set up
    es = EPANetSimulation(path)
    n = es.network.nodes

    # Get patterns
    # S0/Base
    p1_index = 1
    p1 = es.network.patterns[p1_index]
    p1_list = list(p1.values())[0:24]
    pat1 = p1_list

    # S1/Conservation
    p3_index = 3
    p3 = es.network.patterns[p3_index]
    p3_list = list(p3.values())[0:24]

    # Set default pattern = S0 = pattern index 1
    ret = es.ENsetpattern(p1_index, p1_list)
    if ret != 0:
        print ret

    # Get list of nodes
    node_list = np.array(list(n))

    #--------------------------------#
    #-----ASSIGN RANDOM PATTERNS-----#
    #--------------------------------#

    # Get # of nodes to sample (based on the percentage) as an integer
    k = int((percent/100.0)*len(node_list))

    # Get list of randomly sampled % of nodes
    random_sample = random.sample(node_list, k)

    # Get list of nodes NOT in the random sample
    other = [node for node in node_list if node not in random_sample]

    # For random sample list, reset demand pattern = S1 = pattern index 3
    for node in random_sample:
        # Set demand pattern
        ret = es.ENsetnodevalue(node, pat, p3_index)
        if ret != 0:
            print ret

    # After set new patterns for random nodes...
    # Write changes permanently to new temp file (which will be rewritten)
    ret = es.ENsaveinpfile('tmp.inp')
    if ret != 0:
        print ret

    # Open new file of revised network
    es_new = EPANetSimulation('tmp.inp')
    n = es_new.network.nodes
    l = es_new.network.links

    # Run new simulations
    error = es_new.run()
    errorq = es_new.runq()
    if error != None or errorq != None:
        print "Errors: ", error, errorq

#    # Testing
#    print es1.ENgetnodevalue(random_sample[0],pat)
#    print es1.ENgetnodevalue(other[0],pat)

    #--------------------------------#
    #-----GET EPANET SIM RESULTS-----#
    #--------------------------------#

    # Lists of elements
    junc_list = [node for node in list(n) \
                 if n[node].node_type==0 and 'J' in n[node].id]
    res_list = np.array([node for node in list(n) if n[node].node_type==1])
    pump_list = np.array([link for link in list(l) if l[link].link_type==2])
    tank_list = np.array([node for node in list(n) if n[node].node_type==2])
    pipe_list = np.array([pipe for pipe in list(l) if l[pipe].link_type==1])
    valve_list = np.array([link for link in list(l) if l[link].link_type in range(3,9)])

    # Extract correct times
    ts_index = er.getTimeSteps(es_new.network.tsteps)

    #---------------------------#
    #-----CALCULATE METRICS-----#
    #---------------------------#

    # Get metrics

    # Max age (hr)
    age_24hr_med = er.ageResults(junc_list,n,l,ts_index)
    max_age = age_24hr_med #(hr), median age @ 24 hr in system

    # Energy input (gpd * ft)
    # get Hd for reservoirs
    resUser_Hd, resSource_Hd = er.HdResults(res_list,ts_index,n) # [gal/30min * ft]
    resSource_Hd_sum = sum(resSource_Hd)*30 # [gpd * ft]
    # get hlQ for pumps
    pumpUser_hlQ, pumpSource_hlQ = er.hlQResults(pump_list,ts_index,l) # [gal/30min * ft]
    pump_hlQ_sum = sum(pumpSource_hlQ)*30 # [gpd * ft]
    # get Hd for tanks-out
    tankUser_Hd, tankSource_Hd = er.HdResults(tank_list,ts_index,n) # [gal/30min * ft]
    tankSource_Hd_sum = sum(tankSource_Hd)*30 # [gpd * ft]
    # sum for Pin
    energy_in = resSource_Hd_sum + pump_hlQ_sum + tankSource_Hd_sum # [gpd * ft]

    # Energy loss (gpd * ft)
    # get hlQ for pipes
    pipeUser_hlQ, pipeSource_hlQ = er.hlQResults(pipe_list,ts_index,l) # [gal/30min * ft]
    pipe_hlQ_sum = sum(pipeUser_hlQ)*30 # [gpd * ft]
    # get hlQ for valves
    valveUser_hlQ, valveSource_hlQ = er.hlQResults(valve_list,ts_index,l) # [gal/30min * ft]
    valve_hlQ_sum = sum(valveUser_hlQ)*30 # [gpd * ft]
    # sum Plosses
    energy_loss = pipe_hlQ_sum + valve_hlQ_sum # [gpd * ft]

    # Peak flow (gpm)
    peak_flows_by_pipe = er.flowResults(l,ts_index)
    peak_flow = np.median(peak_flows_by_pipe) #(gpm), median peak flow in system

    # Water loss (gpd)
    tot_back_leakage = er.leakResults2(junc_list,n,l,ts_index)
    water_loss = tot_back_leakage*30 #(gpd), total background leakage in system

    # Add metrics to a list of 30
    max_age_results_list.append(max_age)
    energy_in_results_list.append(energy_in)
    energy_loss_results_list.append(energy_loss)
    peak_flow_results_list.append(peak_flow)
    water_loss_results_list.append(water_loss)

    #-----------------#
    #-----END SIM-----#
    #-----------------#

    # Can I force close or clean the open files??
    es.clean()
    es_new.clean()

#-------------------------#
#-----RESULTS SUMMARY-----#
#-------------------------#

# Age min, med, max
max_age_min = [np.min(max_age_results_list)]
max_age_med = [np.median(max_age_results_list)]
max_age_max = [np.max(max_age_results_list)]

# Energy input min, med, max
energy_in_min = [np.min(energy_in_results_list)]
energy_in_med = [np.median(energy_in_results_list)]
energy_in_max = [np.max(energy_in_results_list)]

# Energy loss min, med, max
energy_loss_min = [np.min(energy_loss_results_list)]
energy_loss_med = [np.median(energy_loss_results_list)]
energy_loss_max = [np.max(energy_loss_results_list)]

# Peak flow min, med, max
peak_flow_min =[ np.min(peak_flow_results_list)]
peak_flow_med = [np.median(peak_flow_results_list)]
peak_flow_max = [np.max(peak_flow_results_list)]

# Water loss min, med, max
water_loss_min = [np.min(water_loss_results_list)]
water_loss_med = [np.median(water_loss_results_list)]
water_loss_max = [np.max(water_loss_results_list)]

#------------------------------#
#-----STORE RESULTS: EXCEL-----#
#------------------------------#

df_filename = "ResponseCurveData_EPANET"+net+".xlsx"
if percent == 0:
    df_append = pd.DataFrame({'%': percent, \
                              'Age_24hr_min_hr': max_age_min, \
                              'Age_24hr_med_hr': max_age_med, \
                              'Age_24hr_max_hr': max_age_max, \
                              'Energy_In_min_gpd': energy_in_min, \
                              'Energy_In_med_gpd': energy_in_med, \
                              'Energy_In_max_gpd': energy_in_max, \
                              'Energy_Loss_min_gpd': energy_loss_min, \
                              'Energy_Loss_med_gpd': energy_loss_med, \
                              'Energy_Loss_max_gpd': energy_loss_max, \
                              'Peak_Flow_min_gpm': peak_flow_min, \
                              'Peak_Flow_med_gpm': peak_flow_med, \
                              'Peak_Flow_max_gpm': peak_flow_max, \
                              'Water_Loss_min_gpd': water_loss_min, \
                              'Water_Loss_med_gpd': water_loss_med, \
                              'Water_Loss_max_gpd': water_loss_max})
    df_excel = df_append
    df_excel.to_excel(df_filename, sheet_name='Sheet1', index=False)

else:
    df_append = pd.DataFrame({'%': percent, \
                              'Age_24hr_min_hr': max_age_min, \
                              'Age_24hr_med_hr': max_age_med, \
                              'Age_24hr_max_hr': max_age_max, \
                              'Energy_In_min_gpd': energy_in_min, \
                              'Energy_In_med_gpd': energy_in_med, \
                              'Energy_In_max_gpd': energy_in_max, \
                              'Energy_Loss_min_gpd': energy_loss_min, \
                              'Energy_Loss_med_gpd': energy_loss_med, \
                              'Energy_Loss_max_gpd': energy_loss_max, \
                              'Peak_Flow_min_gpm': peak_flow_min, \
                              'Peak_Flow_med_gpm': peak_flow_med, \
                              'Peak_Flow_max_gpm': peak_flow_max, \
                              'Water_Loss_min_gpd': water_loss_min, \
                              'Water_Loss_med_gpd': water_loss_med, \
                              'Water_Loss_max_gpd': water_loss_max})

    dfRead = pd.read_excel(df_filename)
    frames = [dfRead, df_append.reset_index(drop=True)]
    df_excel = pd.concat(frames,axis=0)

    writer = pd.ExcelWriter(df_filename)
    df_excel.to_excel(writer, sheet_name='Sheet 1', index=False)
    writer.save()